
#!/usr/bin/env python

import CreateGraph


SECRET_KEY = "secret"


from flask import Flask, render_template, request, redirect, url_for, flash, make_response
from flask_bootstrap import Bootstrap
from SPARQLWrapper import SPARQLWrapper, JSON
from rdflib.plugins.stores import sparqlstore
import rdflib as rl
from rdflib.namespace import DC, Namespace, XSD
from rdflib import Literal
import time
import os
import uuid
import base64
import flask
import flask_wtf
import datetime as dt
import json
import shapely.wkt
import shapely.geometry
from flask_wtf import FlaskForm
from flask_wtf.csrf import CSRFProtect
from flask_wtf.file import FileField, FileRequired
from wtforms import StringField, FloatField, SelectField, HiddenField, validators, TextAreaField, IntegerField, ValidationError, \
    DecimalField, BooleanField, SelectMultipleField, widgets, TextField, RadioField
from wtforms.widgets import CheckboxInput
from wtforms.fields.html5 import DateField, TimeField
from wtforms.validators import InputRequired, NumberRange
from wtforms_components import read_only
from wtforms.validators import DataRequired
from flask_bootstrap import Bootstrap
from rdflib.plugins.stores import sparqlstore
from SPARQLWrapper import SPARQLWrapper, JSON
import requests
import datetime
import pyautogui
from flask_weasyprint import HTML, render_pdf
from query import *
import json
import jinja2
from flask import session
import glob
import wtforms
from wtforms.validators import NumberRange
import pdfkit
from jinja2 import Template
from jinja2 import Environment, FileSystemLoader

query_endpoint = "http://localhost:3030/cropprofile/query"
update_endpoint = "http://localhost:3030/cropprofile/update"

BACKEND = sparqlstore.SPARQLUpdateStore()
BACKEND.open((query_endpoint, update_endpoint))

# Meta functions ##############################################################
GEO = Namespace("http://www.opengis.net/ont/geosparql#")
one = dict()
imagedata = ""
file_path = ""
wktdata = ""
enduser = ""
htmlelement = ""
detailslatest = ""
templatevalue = ""
templateall = []
file_name = "complete_template.json"
queryfile_name = "Alltopics_query.json"
ONTOLOGY = "CropLatest.owl"

class GeoRDFStore(object):
    def __init__(self, ontology_filename):
        self._read_ontology(ontology_filename)

    @staticmethod
    def _strip_namespace(literal):
        """ Helper function to remove the namespace prefix."""
        return str(literal).split('#')[-1]

    def _read_ontology(self, filename):
        """ Parses an ontology for user-defined actvities."""
        # ont = rl.Graph()
        # ont.parse(filename)
        #
        # # search activities
        # rdfs = Namespace("http://www.w3.org/2000/01/rdf-schema#")
        # base = Namespace("http://www.semanticweb.org/ontologies/2019/11/activity#")
        # owl = Namespace("http://www.w3.org/2002/07/owl#")
        self._activities = {}
        # for s, p, o in ont.triples((None, rdfs.subClassOf, base.Activities)):
        #     name = GeoRDFStore._strip_namespace(s)
        #     self._activities[name] = []
        #     for s2, p, o in ont.triples((None, rdfs.subClassOf, s)):
        #         self._activities[name].append(GeoRDFStore._strip_namespace(s2))
        # for intermediate, _, field in ont.triples((None, owl.onProperty, None)):
        #     for activity, _, intermediate in ont.triples((None, rdfs.subClassOf, intermediate)):
        #         activity = GeoRDFStore._strip_namespace(activity)
        #         field = GeoRDFStore._strip_namespace(field)
        #         if activity not in self._activities:
        #             self._activities[activity] = []
        #         self._activities[activity].append(field)

        # self._activities = {'ApplyingFertilizer': ['Fieldname', 'appliedMineralFertilizerAgent', 'appliedFertilizerAmountInKgPerHa', 'appliedFertilizerAmountInDtPerHa', 'appliedMineralFertilizerNitrogenContentInKgPerHa', 'Personname','Comments'],
        #                     'Harvesting': ['Fieldname', 'harvestedCrop', 'harvestedQuantityInDtPerHa', 'transportedHarvest', 'Machinename', 'Personname', 'Comments'],
        #                     'Plantation': ['Fieldname', 'Comments', 'sownCrop', 'sownQuantityInKgPerHa', 'appliedPesticideAgent', 'appliedPesticideAmountInLPerHa','Personname', 'Comments'],
        #                     'Seeding': ['Fieldname', 'Personname', 'Machinename','Comments'],
        #                     'Tilling': ['Fieldname', 'appliedTillage', 'Machinename', 'Personname', 'Comments']
        #                     }

        # with open("Activities.json", "r+") as file:
        #     self._activities = json.load(file)

        one = {}
        with open("AllActivities.json", "r+") as activityfile:
            activities = json.load(activityfile)

            for i in activities["Activities"]:
                one[i["ActivityName"]] = i["ActivitySubFields"]

        #self._activities = one
        #print(self._activities)
        #print(one)

        self._activities = one

        # with open("Activities.json", "r+") as activityfile:
        #     activitydata = json.load(activityfile)
        #
        #     activityfile.seek(0)
        #     json.dump(activitydata, activityfile, indent=4)
        #     activityfile.truncate()
        #
        # print(activitydata)
        # for i in activitydata:
        #     print(i)
        #     print(activitydata[i])
        #
        # initial_data = {}
        # initial_data["Activities"] = []
        # initial_data["Activities"].append({
        #     "ActivityName": "",
        #     "ActivitySubFields": ""
        # })
        #
        # with open("AllActivities.json", "r+") as allactivityfile:
        #     allactivitydata = json.load(allactivityfile)
        #
        #     #print("allactivitydata")
        #     #for i in activitydata:
        #     #    allactivitydata["Activities"].append({"ActivityName": "%s"%(i), "ActivitySubFields": ""})
        #
        #     for j in allactivitydata["Activities"]:
        #         print(j)
        #         j["ActivitySubFields"] = activitydata[j['ActivityName']]
        #
        #     allactivityfile.seek(0)
        #     json.dump(allactivitydata, allactivityfile, indent=4)
        #     allactivityfile.truncate()

        #print(self._activities)

        #self._activity_namespace = base

    def get_activities(self):
        """ Returns a list of all known activities."""
        return sorted(self._activities.keys())

    def get_activity_fields(self, activity):
        """ For a given activity, returns the fields specified json."""
        return self._activities[activity][:]

    def assign_graph(self, graph):
        """ Attach a SPARQL-backed graph store."""
        self._graph = graph

    @staticmethod
    def wkt2geojson(wkt):
        """ Converts WKT to GeoJSON."""
        g1 = shapely.wkt.loads(wkt)
        g2 = shapely.geometry.mapping(g1)
        return json.dumps(g2)

    def add_image(self, activitynew, **kwargs):
        # create identifier
        #image = rl.URIRef("http://project/base/" + "N" + uuid.uuid4().hex)

        image = rl.URIRef("http://Logjournal" + "DateTime" + str(datetime.datetime.now().time()))

        example = Namespace("http://example.org/#")

        #self._graph.add((image, rl.RDF.type, rl.Literal("Logjournal")))

        self._graph.add((image, rl.RDF.type, example["LogJournal"]))

        #save data
        # self._graph.add((image, rl.RDF.type, rl.Literal("image")))
        # self._graph.add((image, rl.RDF.type, rl.RDFS.Class))
        #
        #
        # for name, value in kwargs.items():
        #     print(name)
        #     print(value)
        #     if name == "imagedata":
        #         self._graph.add((image, DC["source"], rl.Literal(value)))
        #         continue
        #     if name == "wkt":
        #         self._graph.add((image, GEO["asWKT"], rl.Literal(value)))
        #         continue
        #     self._graph.add((image, self._activity_namespace[name], rl.Literal(value)))

        ex = Namespace("http://example.org/#")
        #self._graph.add((image, ex[activitynew], rl.Literal("Logjournal")))
        for name, value in kwargs.items():
            #print(name)
            #print(value)
            #print(value)
            if name == "activity":
                self._graph.add((image, ex[name], ex[value]))
                continue

            if name == "imagedata":
                self._graph.add((image, DC["source"], rl.Literal(value)))
                continue

            if name == "wkt":
                self._graph.add((image, GEO["asWKT"], rl.Literal(value)))
                #self._graph.add((image, ex[name], rl.Literal(value, datatype=GEO.wktLiteral)))
                continue

            self._graph.add((image, ex[name], rl.Literal(value)))

        # save bounding box
        shape = shapely.wkt.loads(kwargs['wkt'])
        bounding_box = shape.bounds
        for idx, pos in enumerate('minx miny maxx maxy'.split()):
            self._graph.add((image, GEO[pos], rl.Literal(bounding_box[idx])))

    def _do_query(self, query):
        """ Run a SPARQL query. """
        response = requests.post(query_endpoint, data={'query': query})
        #print("result"%(response.json()['results']['bindings']))
        try:
            return response.json()['results']['bindings']
        except:
            return flask.render_template("page1.html")

#     def query_wkt(self, wkt):
#         """ Query objects from the RDF database."""
#
#         query = """
# PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
# PREFIX : <http://example.org/#>
#
# SELECT ?predicate ?object ?predicate1 ?object1
# WHERE {
#   GRAPH <http://example.org/#>
#   {
#
#   :Harvesting ?predicate ?object
#   :ApplyingFertilizer ?predicate1 ?object1
# }
# }"""
#         result = self._do_query(query)
#         for activity in result:
#             print(activity)
#             try:
#                 prefix, field = activity['predicate']['value'].split('#')
#                 if field == "type":
#                     pass
#                 else:
#                     #print(field)
#                     if activity['object']['value'] != '':
#                         #print(activity)
#                         field1 = activity['object']['value']
#                         #print(field1)
#                         one[field] = field1
#             except:
#                 one['image'] = activity['object']['value']
#
#             #one.append(activity['subject']['value'].replace('http://example.org/#', ''))
#             #one.update(Activity=activity['subject']['value'].replace('http://example.org/#', ''))
#             #one['Activity'] = activity['subject']['value'].replace('http://example.org/#', '')
#             #print(result)
#
#         #print(one)
#
#         return one

    def query_date(self, fromdate, todate):
        """ Query objects with date """

        #print(fromdate)
        #print(todate)
        dateresults = []

        queryfordate = query19 % (fromdate, todate)
        #print(queryfordate)
        for result in self._do_query(queryfordate):
            dateresults.append(result['subject']['value'])

        return dateresults

    def query_wkt(self, wkt):
        """ Query objects from the RDF database that intersect with wkt."""

        # Fetch any object whose bounding box intersects with the bounding box of the query
        shape = shapely.wkt.loads(wkt)
        #print(shape)
        minx, miny, maxx, maxy = shape.bounds

        query = """
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT ?subject ?wkt
WHERE {
  GRAPH <http://project/base/defaultgraph> {
  ?subject geo:miny ?miny
    FILTER (
      ?miny <= "%f"^^xsd:double
    )
  ?subject geo:maxy ?maxy
    FILTER (
      ?maxy >= "%f"^^xsd:double
    )
  ?subject geo:minx ?minx
    FILTER (
      ?minx <= "%f"^^xsd:double
    )
  ?subject geo:maxx ?maxx
    FILTER (
      ?maxx >= "%f"^^xsd:double
    )
  ?subject geo:asWKT ?wkt
  }
}""" % (maxy, miny, maxx, minx)

        for result in self._do_query(query):
            print(result['subject']['value'])

        #query = query1
        # Check each of the results for actual intersection, supports arbitrary shapes
        found = []
        for result in self._do_query(query):
            elementshape = shapely.wkt.loads(result['wkt']['value'])
            if shape.contains(elementshape):
            #if shape.almost_equals(elementshape):
            # Check for intersection
            #if shape.intersects(elementshape):
                #print("Contains")
                found.append(result['subject']['value'])

            if shape.almost_equals(elementshape):
                #print("Almost Equals")
                found.append(result['subject']['value'])

            if shape.equals(elementshape):
                #print("Equals")
                found.append(result['subject']['value'])

            if shape.intersection(elementshape):
                found.append(result['subject']['value'])

        found = list(dict.fromkeys(found))

        print(found)


        return found

    def get_details(self, identifier):
        """ Return all known information for given entry given by its identifier."""
        query = query2 % identifier

        keeplist = [rl.RDF, DC, Namespace('http://example.org/#')]
        keeplist = [str(_).strip('#') for _ in keeplist]

        found = dict()

        for result in self._do_query(query):
            #print(result)
            try:
                prefix, field = result['predicate']['value'].split('#')
                if prefix in keeplist:
                    if field == 'type':
                        pass
                    else:
                        found[field] = result['object']['value']
            except:
                found['image'] = result['object']['value']

        return found

    def get_details1(self, identifier):
        """ Return all known information for given entry given by its identifier."""
        query = query3 % identifier

        #print("identifier")
        #print(identifier)
        keeplist = [rl.RDF, DC, Namespace('http://example.org/#')]
        keeplist = [str(_).strip('#') for _ in keeplist]

        found = dict()

        for result in self._do_query(query):

            try:
                prefix, field = result['predicate']['value'].split('#')
                if prefix in keeplist:
                    if field == 'type':
                        pass
                    else:
                        found['LogJournal'] = result['subject']['value']
                        #found[result['predicate']['value']] = result['object']['value']
                        found[field] = result['object']['value']
            except:
                found['image'] = result['object']['value']

            # try:
            #     prefix, field = result['predicate']['value'].split('#')
            #     if prefix in keeplist:
            #         if field == 'type':
            #             pass
            #         else:
            #             found['LogJournal'] =  result['subject']['value']
            #             found[field] = result['object']['value']
            # except:
            #     found['image'] = result['object']['value']

        #print(found)
        return found


###### Web Interface

# query_endpoint = 'http://localhost:3030/ds/query'
# update_endpoint = 'http://localhost:3030/ds/update'
# store = sparqlstore.SPARQLUpdateStore()
# store.open((query_endpoint, update_endpoint))
# STORE = GeoRDFStore(ONTOLOGY)

app = Flask(__name__)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
#print("application1")
#print(app)
#app = flask.Flask("rdfweb", static_url_path="")
#print("application2")
#print(app)
app.config["SECRET_KEY"] = SECRET_KEY
#csrf = CSRFProtect(app)
#csrf.init_app(app)
Bootstrap(app)
STORE = GeoRDFStore(ONTOLOGY)
# Bootstrap(app)


class ActivityForm(FlaskForm):
    """ Form to add an activity."""

    #print(STORE.get_activities())
    #activity = SelectField("Please Choose The Activity", choices=[(_, _) for _ in STORE.get_activities()])
    activity = SelectField("<b>Please Choose The Activity<b>", choices=[(_, _) for _ in  \
              ['ApplyingFertilizer', 'Harvesting', 'Plantation', 'Seeding', 'Tilling']])
    wkt = HiddenField()
    #wkt = HiddenField(None, validators=[DataRequired(message="Please Choose The Location")])
    #print("----2-----")


class BaseForm(FlaskForm):
    wkt = HiddenField()
    #wkt = HiddenField(None, validators=[DataRequired(message="Please Choose The Location")])
    _readonly_fields = []
    _editonly_fields = []

    def add_timestamping(self):
        setattr(self, "date", DateField("Date", default=dt.datetime.now()))
        setattr(self, "time", TimeField("Time", default=dt.datetime.now()))

    def add_activity(self, default):
        setattr(self, "activity", HiddenField(default=default))

    def add_textfield(self, name):
        if "Amount" in name or "amount" in name or "content" in name or "Content" in name:
            #setattr(self, name, IntegerField('test', validators=[wtforms.validators.number_range(min=0, max=200, message='unacceptable.')]))

            setattr(self, name, DecimalField(name, validators=[DataRequired(message="Please Choose The Numbers"), NumberRange(0, 200, message="Hello")]))

            #setattr(self, name, IntegerField(name, validators=[InputRequired()]))
            #ef validate_name(FlaskForm, name):
            #    if 0 > name.data > 200:
            #        raise ValidationError('Name must be less than 50 characters')
        else:
            setattr(self, name, StringField(name))

    def add_image(self, content):
        if content is None:
            #print("content is none")
            setattr(self, "image", FileField("Image"))
        else:
            #print("content is there")
            setattr(self, "image", HiddenField(default=content))

    @staticmethod
    def for_activity(activity, readonly, editonly, imagecontent=None):
        class DynamicForm(BaseForm):
            pass

        form = DynamicForm
        BaseForm.add_timestamping(form)
        BaseForm.add_activity(form, activity)
        form._readonly_fields = []
        form._editonly_fields = []

        #print(activity)
        for field in STORE.get_activity_fields(activity):
            BaseForm.add_textfield(form, field)
            if readonly:
                form._readonly_fields.append(field)
            if editonly:
                form._editonly_fields.append(field)

        #print(imagecontent)
        BaseForm.add_image(form, imagecontent)

        if readonly:
            form._readonly_fields += 'date time'.split()

        if editonly:
            form._editonly_fields += 'date time'.split()

        return form

    def __init__(self, *args, **kwargs):
        super(BaseForm, self).__init__(*args, **kwargs)
        for field in self._readonly_fields:
            getattr(self, field).render_kw = {'readonly': 'readonly'}

        for field in self._editonly_fields:
            getattr(self, field).render_kw = {'editonly': 'editonly'}


class QueryForm(FlaskForm):
    #wkt = HiddenField()
    topicsOfInterests = SelectField("<b>Please Choose The Topics of Interests<b>", choices=[(_, _) for _ in \
               ['All Activities', 'FertilizerDetails', 'HarvestingDetails','CropDetails', 'SeedingDetails',
                'TillingDetails', 'All Crops']])

class QueryForm1(FlaskForm):
    #wkt = HiddenField()
    topicsOfInterests = SelectField("<b>Please Choose The Topics of Interests<b>", choices=[(_, _) for _ in \
               ['All Activities', 'FertilizerDetails', 'HarvestingDetails', 'TillingDetails', 'All Crops']])

class QueryForm2(FlaskForm):
    #wkt = HiddenField()
    topicsOfInterests = SelectField("<b>Please Choose The Topics of Interests<b>", choices=[(_, _) for _ in \
               ['All Activities', 'CropDetails', 'SeedingDetails', 'TillingDetails', 'All Crops']])


class QueryForm3(FlaskForm):
    #wkt = HiddenField()
    topicsOfInterests = SelectField("<b>Please Choose The Topics of Interests<b>", choices=[(_, _) for _ in \
               ['All Activities', 'FertilizerDetails', 'HarvestingDetails','CropDetails', 'SeedingDetails']])


class QuerySelctionForm(FlaskForm):
    wkt = HiddenField(None, validators=[DataRequired(message="Please Choose The Location")])
    dateFrom = DateField("From-Date", default=dt.date(2020, 1, 1))
    dateTo = DateField("To-Date", default=dt.datetime.now())
    # language = SelectField('<b>Please Choose The Output Format<b>', choices=[('html', 'HTML'), \
    #                       ('pdf', 'PDF'), ('latex', 'Latex')])


class QuerySelctionForm1(FlaskForm):
    #example = RadioField('Please Choose The Output Format', choices=[('HTML', 'HTML'), ('PDF', 'PDF')])
    language = SelectField('<b>Please Choose The Output Format<b>',choices=[('html', 'HTML'), \
                          ('pdf', 'PDF'), ('latex', 'Latex')])


class OutputSelectionForm(FlaskForm):
    #checkbox = BooleanField('One')
    # outputselection = SelectField("<b>Please Choose The Output Format<b>", choices=[(_, _) for _ in \
    #                               ['html', 'PDF', 'Latex', 'CSV', 'XML']])

    assignedTopicOfInterest = StringField('TopicOfInterest:', validators=[validators.required()])


class CustomQueryForm(FlaskForm):
    wkt = HiddenField(None, validators=[DataRequired(message="Please Choose The Location")])
    defaultData = (
                  "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \n"
                  "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n"
                  "PREFIX geo: <http://www.opengis.net/ont/geosparql#> \n"
                  "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \n"
                  "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"
                  "PREFIX ex: <http://example.org/#> \n"
                  "SELECT ?subject ?predicate ?object \n"
                  "WHERE { \n"
                  "GRAPH <http://project/base/defaultgraph> { \n"
                  "**** Location Filter **** \n" 
                  "?subject ?predicate ?object .  \n"
                  "} } \n"
                  )

    querryfield = TextAreaField(u'<br><b>Please Write Down The Query<b>', default=defaultData,
                                render_kw={'class': 'form-control', 'rows': 12}, validators=[DataRequired()])


class NewTemplateForm(FlaskForm):
    newTemplatename = StringField('Please Write Down New Template Name')


class TemplateSelectionForm(FlaskForm):

    templateSelection = SelectField("<b>Please Choose The Template<b>", choices=[(_, _) for _ in \
                                                          ['Template1', 'Template2', 'Template3']])


class ActivityDetailForm(FlaskForm):
    wkt = HiddenField()
    activity = HiddenField()
    date = DateField("Date", default=dt.datetime.now())
    time = TimeField("Time", default=dt.datetime.now())
    fertilzerName = StringField("FertizerName")
    fertilzerQuantity = StringField("FertilzerQuantity")
    personName = StringField("PersonName")
    image = FileField("Image")


class ActivityReviewForm(FlaskForm):
    wkt = HiddenField()
    activity = HiddenField()
    date = DateField("Date", default=dt.datetime.now())
    time = TimeField("Time", default=dt.datetime.now())
    fertilzerName = StringField("FertizerName")
    fertilzerQuantity = StringField("FertilzerQuantity")
    personName = StringField("PersonName")
    imagedata = HiddenField()

    def __init__(self, *args, **kwargs):
        super(ActivityReviewForm, self).__init__(*args, **kwargs)
        self.date.render_kw = {'readonly': 'readonly'}
        self.time.render_kw = {'readonly': 'readonly'}
        self.fertilzerName.render_kw = {'readonly': 'readonly'}
        self.fertilzerQuantity.render_kw = {'readonly': 'readonly'}
        self.personName.render_kw = {'readonly': 'readonly'}


def create_graph():
    #print('Inside create graph')
    GRAPH = rl.Graph(BACKEND, identifier="http://project/base/defaultgraph")
    GRAPH.bind("geo", GEO)

    # LOAD ONTOLOGY SCHEMA
    example = Namespace("http://example.org/#")

    GRAPH.add((example["Activities"], rl.RDF.type, rl.RDFS.Class))
    GRAPH.add((example["LogJournal"], rl.RDF.type, rl.RDFS.Class))

    # with open("Activities.json", "r+") as file:
    #     namedActivities = json.load(file)

    namedActivities = []
    with open("AllActivities.json", "r+") as abcfile:
        oneact = json.load(abcfile)

        for i in oneact["Activities"]:
            namedActivities.append(i["ActivityName"])

    #print(namedActivities)
    #namedActivities = list(namedActivities.keys())
    #print(namedActivities)
    #namedActivities = ['ApplyingFertilizer', 'Tilling', 'Seeding', 'Harvesting', 'Plantation']

    for activityname in namedActivities:
        #print(example[activityname])
        GRAPH.add((example[activityname], rl.RDF.type, example["Activities"]))

    for activityname in namedActivities:
        activity_fields = STORE.get_activity_fields(activityname)
        #print(activity_fields)
        for field in activity_fields:
            #print(field)
            GRAPH.add((example[field], rl.RDF.type, rl.RDF.Property))
            GRAPH.add((example[field], rl.RDFS.domain, example["LogJournal"]))
            #GRAPH.add((example[field], rl.RDFS.domain, example[activityname]))
            #GRAPH.add((example[field], rl.RDFS.range, example[activityname]))
            GRAPH.add((example[field], rl.RDFS.range, rl.RDFS.Literal))

    # GRAPH.add((example["ApplyingFertilizer"], rl.RDF.type, example["Activities"]))
    # GRAPH.add((example["Tiliing"], rl.RDF.type, example["Activities"]))
    # GRAPH.add((example["Seeding"], rl.RDF.type, example["Activities"]))
    # GRAPH.add((example["Harvesting"], rl.RDF.type, example["Activities"]))
    # GRAPH.add((example["Plantation"], rl.RDF.type, example["Activities"]))
    # GRAPH.add((example["WateringPlants"], rl.RDF.type, example["Activities"]))

    #print(GRAPH)
    STORE.assign_graph(GRAPH)
    #print(STORE)

@jinja2.contextfunction
def get_context(c):
    return c

@app.route('/', methods=['GET', 'POST'])
def hello_world():
    #print("program starts")
    create_graph()

    routing = ""
    if request.method == 'POST':
        if request.form.get('Farmer') == 'Farmer':
            # routing = "page2"
            # return redirect(url_for('choose_location'))
            return redirect(url_for('enter_logs'))
        elif request.form.get('EndUsers') == 'EndUsers':
            return redirect(url_for('choose_end_user'))
        # elif request.form.get('CustomQuery') == 'CustomQuery':
        #     return redirect(url_for('customquerynew'))
        # elif request.form.get('View/Edit-Templates') == 'View/Edit-Templates':
        #     return redirect(url_for('edit_templates'))
        elif request.form.get('Developer') == 'Developer':
            return redirect(url_for('developer'))

    return render_template("page1.html", title="Activity")


@app.route('/developer', methods=['GET', 'POST'])
def developer():
    if request.method == 'POST':
        if request.form.get('Configure - Query and Topics') == 'Configure - Query and Topics':
            return redirect(url_for('customquerynew'))
        elif request.form.get('Modify - CustomQuery and Topic') == 'Modify - CustomQuery and Topic':
            return redirect(url_for('edit_topics'))
        elif request.form.get('Configure EndUser Templates') == 'Configure EndUser Templates':
            return redirect(url_for('edit_templates_new'))
        elif request.form.get('Add Visualization Template') == 'Add Visualization Template':
            return redirect(url_for('add_templates'))
        elif request.form.get('Choose Visu Template and Topics') == 'Choose Visu Template and Topics':
            return redirect(url_for('configure_reports'))
        elif request.form.get('Configure Report - Templates') == 'Configure Report - Templates':
            return redirect(url_for('edit_templates'))

    return render_template("developer.html")


@app.route("/configure_reports", methods=["GET", "POST"])
def configure_reports():

    templist = glob.glob("templates/Visualization/*.html")
    x = []
    for i in templist:
        x.append(i.replace("templates/Visualization/", ""))

    return render_template("developer.html", route="configure_reports")


@app.route("/add_templates", methods=["GET", "POST"])
def add_templates():
    #print(request.form.get('topicsofinterest'))

    templist = glob.glob("templates/Visualization/*.html")
    x = []
    for i in templist:
        x.append(i.replace("templates/Visualization/", ""))

    #print(x)

    if request.method == "POST":
        #print("again here")
        #print(request.form.get('newTemplatename'))
        #print("topicvalue")
        #print(request.form.get('newtopicvalue'))
        #print(request.form.get('topicsofinterest'))

        #if request.form.get('submit') == "Submit":
            #print("Submitted")
            #print(request.form.get('topicsofinterest'))
            #print(request.form.get('topicsofinterestdeletion'))

        if request.form.get('deleteTemplatename') != '' and request.form.get('deleteTemplatename') is not None:
            if os.path.exists("templates/Visualization/%s.html"%(request.form.get('deleteTemplatename'))):
                os.remove("templates/Visualization/%s.html"%(request.form.get('deleteTemplatename')))

            return flask.render_template("page_14.html")

        if request.form.get('newTemplatename') != '' and request.form.get('newTemplatename') is not None:
            #print("create template")
            html_str = """
{% for key, value in resultsupdated.items() %}
{{key}} : {{value}}
{% endfor %}
            """

            cwd = os.getcwd()
            #print(cwd)

            #print(request.form.get('newTemplatename'))

            Html_file = open("templates/Visualization/%s.html" % (request.form.get('newTemplatename')), "w")
            Html_file.write(html_str)
            Html_file.close()

            with open("templates/page_13.html", "r", encoding='utf-8') as in_file:
                buf = in_file.readlines()

            with open("templates/page_13.html", "w") as out_file:
                for line in buf:
                    #print(line)
                    if line == "Text needs to be replaced\n":
                        #print("reached inside")
                        line = line + """
<div align="center" style="display: block" id="results2">
%s
</div>"""%(html_str)

                    out_file.write(line)

            return flask.render_template("page_14.html")

        if request.form.get('newtopicvalue') is None:
            for element in x:
                if request.form.get(element) == element:
                    with open("templates/Visualization/%s"%(element), "r", encoding='utf-8') as f:

                        text = f.read()

                    return render_template("edittemplates.html", templatevalue=element, htmltext=text, route="add_templates")

        if request.form.get('newtopicvalue') is not None:
            html_str = request.form.get('newtopicvalue')
            #print(html_str)
            filename = request.form.get('newtopicname')
            #print(filename)
            #print(html_str)
            #print(filename)

            cwd = os.getcwd()
            #print(cwd)

            #print(request.form.get('newtopicname'))

            Html_file = open("templates/Visualization/%s"%(request.form.get('newtopicname')), "w")
            Html_file.write(html_str)
            Html_file.close()

            return flask.render_template("page_14.html")


    #return flask.render_template("page_23.html", visutemplates=x, display=False, route="add_templates")

    return flask.render_template("addtemplates.html", visutemplates=x, route="add_templates")


@app.route("/enter-logs", methods=["GET", "POST"])
def enter_logs():
    # store form?
    #form = ActivityReviewForm()
    create_graph()

    activity = flask.request.form.get('activity')
    message = ""

    #print("-----1------")

    #torrent_id = request.form["update"]
    #print(torrent_id)

    if activity is not None:
        form = BaseForm.for_activity(activity, False, False, " ")()

        if form.validate_on_submit():

            if "update" in request.form:
                #print("I pressed Update here")

                geojson = GeoRDFStore.wkt2geojson(form.wkt.data)
                act = form.activity.data

                confirm_form = BaseForm.for_activity(activity, False, True)()

                return flask.render_template("page_2.html", form=confirm_form, prefill=geojson, image=imagedata,
                                             message2=act,
                                             route="enter-confirm")

            elif "submit" in request.form:
                #print("I pressed Submit here")
                #print(content)
                #if form.validate_on_submit():
                    #print("I pressed submit")
                entered = {}
                for fieldname, value in form.data.items():
                    #print(fieldname)
                    #print(value)
                    if fieldname == "csrf_token":
                        continue

                    # if fieldname == 'wkt':
                    #     value = value + "^^<http://www.opengis.net/ont/geosparql#wktLiteral>"
                    #     print(value)
                    #print(value)

                    entered[fieldname] = value

                #print(activity)
                STORE.add_image(activity, **entered)
                message = "Entry is saved to the Database"

    form = ActivityForm()
    return flask.render_template("page_1.html", form=form, route="enter-details", prefill="null", message=message, message2="null")

@app.route("/enter-details", methods=["GET", "POST"])
def enter_details():
    form = ActivityForm()

    #print(form.wkt.data)
    if form.wkt.data == '':
        #flash("Invalid Selection: Please Choose Location and Select The Activity")
        return '<form action="/enter-logs" method="GET"><b>Invalid Selection: Please Choose Location and Select The Activity</b>'\
               '<br><br><input type="submit" value="Go Back"></form>'

    if not form.validate_on_submit():
        return '<form action="/enter-logs" method="GET"><b>Invalid Form Entry: Please Enter Correct Numbers, Location and Select The Activity</b>' \
               '<br><br><input type="submit" value="Go Back"></form>'

    # geojson = GeoRDFStore.wkt2geojson(form.wkt.data)
    # form = ActivityDetailForm(activity=form.activity.data)
    # act = form.activity.data

    activity = form.activity.data
    geojson = GeoRDFStore.wkt2geojson(form.wkt.data)

    #print("----3-----")

    #print("activity")
    #print(activity)

    if form.wkt.data != '':
        form = BaseForm.for_activity(activity, False, False)()
    else:
        return '<form action="/enter-logs" method="GET"><b>Invalid Selection: Please Choose Location and Select The Activity</b>' \
               '<br><br><input type="submit" value="Go Back"></form>'

    return flask.render_template("page_1.html", form=form, route="enter-confirm", prefill=geojson, message2=activity)


@app.route("/enter-confirm", methods=["GET", "POST"])
def enter_confirm():
    # form = ActivityDetailForm()
    # if not form.validate_on_submit():
    #     return "Invalid form"

    activity = flask.request.form.get('activity')
    form = BaseForm.for_activity(activity, False, False, "")()
    if not form.validate_on_submit():
        return '<form action="/enter-logs" method="GET"><b>Invalid Form Entry: Please Enter Correct Amount in Numbers, Choose Location and Select The Activity</b>' \
               '<br><br><input type="submit" value="Go Back"></form>'

    geojson = GeoRDFStore.wkt2geojson(form.wkt.data)
    act = form.activity.data

    content = ''
    if form.image.data:
        fileobj = flask.request.files.get("image")
        content = base64.b64encode(fileobj.read()).decode("utf-8")
        content = "data:%s;base64,%s" % (str(fileobj.content_type), str(content))

    # show readonly form
    # confirm_form = ActivityReviewForm(date=form.date.data, time=form.time.data, person=form.fertilzerName.data, other=form.fertilzerQuantity.data, wkt=form.wkt.data, imagedata=content, activity=form.activity.data)
    # confirm_form.image.data = content

    confirm_form = BaseForm.for_activity(activity, True, False, content)()
    confirm_form.image.data = content
    global imagedata
    imagedata = content

    return flask.render_template("page_2.html", form=confirm_form, prefill=geojson, image=content, message2=act, route="enter-logs")


@app.route('/edit_topics', methods=['GET', 'POST'])
def edit_topics():

    ## Grab all queries and names for the queries ###
    topicsOfInterest = []

    global file_name
    global queryfile_name

    with open(queryfile_name, "r+") as onkfile:
        onkdata = json.load(onkfile)

        for data in onkdata["Topics_and_Queries"]:
            if data["topicname"] != "None":
                topicsOfInterest.append(data["topicname"])

    # with open("query.json", "r+") as file:
    #     data = json.load(file)
    #     for toi in data:
    #         if toi != 'null' and toi != '':
    #             topicsOfInterest.append(toi)

    #print(topicsOfInterest)

    if request.method == "POST":

        #print(request.form.get("topicofinterest"))
        #print(request.form.get("topicofinterestdeletion"))
        #print(request.form.get("newtopicname"))

        if request.form.get("topicofinterestdeletion") != "Select the Topic" and request.form.get("topicofinterestdeletion") is not None:

            #print("here1")
            if request.form.get('submit') == "Submit":
                topicnamedelete = request.form.get("topicofinterestdeletion")

                # with open("query.json", "r+") as queryfile1:
                #     alltopics = json.load(queryfile1)
                #     if topicnamedelete in alltopics:
                #         del alltopics[topicnamedelete]
                #
                #         queryfile1.seek(0)
                #         json.dump(alltopics, queryfile1, indent=4)
                #         queryfile1.truncate()

                with open(queryfile_name, "r+") as queryfile_one:
                    querydataone = json.load(queryfile_one)

                    for j in range(len(querydataone["Topics_and_Queries"])):
                        if querydataone["Topics_and_Queries"][j]["topicname"] == topicnamedelete:
                            querydataone["Topics_and_Queries"].pop(j)
                            break

                    queryfile_one.seek(0)
                    json.dump(querydataone, queryfile_one, indent=4)
                    queryfile_one.truncate()


                with open(file_name, "r+") as complefile:
                    reportdata = json.load(complefile)

                    for report in reportdata["Reports"]:
                        if topicnamedelete in report["intrestedtopics"]:
                            report["intrestedtopics"].remove(topicnamedelete)

                            complefile.seek(0)
                            json.dump(reportdata, complefile, indent=4)
                            complefile.truncate()

                # with open("TemplateInterests.json", "r+") as file:
                #     allinterests = json.load(file)
                #     for key in allinterests:
                #         if topicnamedelete in allinterests[key]:
                #             allinterests[key].remove(topicnamedelete)
                #
                #     file.seek(0)
                #     json.dump(allinterests, file)
                #     file.truncate()

                return flask.render_template("page_14.html")

        if request.form.get("topicofinterest") != "Select the Topic" and request.form.get("topicofinterest") is not None:

            print("here2")
            ## code for displaying query value

            if request.form.get('submit') == "Submit":

                topicName = request.form.get("topicofinterest")
                session['oldtopicname'] = topicName

                for topic in onkdata["Topics_and_Queries"]:
                    if topicName == topic["topicname"]:
                        topicqueryvalue = topic["queryvalue"]
                    else:
                        continue

                #print(data[request.form.get("topicofinterest")])
                #print(topicName)

                topicquerydict = {}
                topicquerydict[topicName] = topicqueryvalue

                return flask.render_template("page_21.html", checkboxes=topicsOfInterest, display=True, \
                                             queryvalue=topicquerydict, route="edit_topics")

        if request.form.get("newtopicname") != "" and request.form.get("newtopicname") is not None:

            print("here3")
            print(request.form.get("newtopicname"))
            print(request.form.get("newtopicvalue"))

            if request.form.get('submit') == "Submit":

                #print(session.get('oldtopicname'))
                #print(request.form.get("newtopicname"))

                with open(queryfile_name, "r+") as xyzquery_file:
                    querytopicdata = json.load(xyzquery_file)

                    for k in range(len(querytopicdata["Topics_and_Queries"])):
                        if querytopicdata["Topics_and_Queries"][k]["topicname"] == request.form.get("newtopicname"):
                            querytopicdata["Topics_and_Queries"][k]["queryvalue"] = request.form.get("newtopicvalue")

                        else:
                            querytopicdata["Topics_and_Queries"].pop(k)

                            querytopicdata["Topics_and_Queries"].append({
                                "topicname": "%s"%(request.form.get("newtopicname")),
                                "queryvalue": "%s"%(request.form.get("newtopicvalue"))
                            })

                            with open(file_name, "r+") as topicmodifyfile:
                                modifydata = json.load(topicmodifyfile)

                                for report in modifydata["Reports"]:
                                    if session.get('oldtopicname') in report["intrestedtopics"]:
                                        # delete old topicname
                                        report["intrestedtopics"].remove(session.get('oldtopicname'))

                                        # append new topicname
                                        report["intrestedtopics"].append(request.form.get("newtopicname"))

                                topicmodifyfile.seek(0)
                                json.dump(modifydata, topicmodifyfile, indent=4)
                                topicmodifyfile.truncate()

                    xyzquery_file.seek(0)
                    json.dump(querytopicdata, xyzquery_file, indent=4)
                    xyzquery_file.truncate()

                    # for j in range(len(querydataone["Topics_and_Queries"])):
                    #     if querydataone["Topics_and_Queries"][j]["topicname"] == topicnamedelete:
                    #         querydataone["Topics_and_Queries"].pop(j)
                    #         break
                    #
                    # queryfile_one.seek(0)
                    # json.dump(querydataone, queryfile_one, indent=4)
                    # queryfile_one.truncate()


                # with open("query.json", "r+") as abcfile:
                #     querydata = json.load(abcfile)
                #     if request.form.get("newtopicname") in querydata:
                #         querydata[request.form.get("newtopicname")] = request.form.get("newtopicvalue")
                #
                #         abcfile.seek(0)
                #         json.dump(querydata, abcfile, indent=4)
                #         abcfile.truncate()
                #
                #     else:
                #         if session.get('oldtopicname') in querydata:
                #             del querydata[session.get('oldtopicname')]
                #
                #             querydata[request.form.get("newtopicname")] = request.form.get("newtopicvalue")
                #
                #             abcfile.seek(0)
                #             json.dump(querydata, abcfile, indent=4)
                #             abcfile.truncate()
                #
                #         with open(file_name, "r+") as topicmodifyfile:
                #             modifydata = json.load(topicmodifyfile)
                #
                #             for report in modifydata["Reports"]:
                #                 if session.get('oldtopicname') in report["intrestedtopics"]:
                #
                #                     #delete old topicname
                #                     report["intrestedtopics"].remove(session.get('oldtopicname'))
                #
                #                     # append new topicname
                #                     report["intrestedtopics"].append(request.form.get("newtopicname"))
                #
                #             topicmodifyfile.seek(0)
                #             json.dump(modifydata, topicmodifyfile, indent=4)
                #             topicmodifyfile.truncate()

                return flask.render_template("page_14.html")

    return flask.render_template("page_21.html", checkboxes=topicsOfInterest, display=False, route="edit_topics")

@app.route('/edit-templates-new', methods=['GET', 'POST'])
def edit_templates_new():
    form = NewTemplateForm()

    # with open("TemplateValues.json", "r+") as file:
    #     data = json.load(file)
    #     templatesall = data["Templates"]

    global file_name
    all_report_names = []

    if os.path.exists(file_name):
        with open(file_name, "r+") as file:
            data = json.load(file)
            reportobject = data["Reports"]
            for report in reportobject:
                all_report_names.append(report["reportname"])
    else:
        initialdata = {}
        initialdata["Reports"] = []
        initialdata["Reports"].append({
            "reportname": "sample",
            "intrestedtopics": "",
            "visualization_template_option" : ""
        })

        with open(file_name, "w") as file:
            json.dump(initialdata, file, indent=4)

    #print(all_report_names)

    # topicsOfInterest = []
    #
    # with open("query.json", "r+") as file:
    #     data = json.load(file)
    #     for toi in data:
    #         if toi != 'null' and toi != '':
    #             topicsOfInterest.append(toi)

    #print(topicsOfInterest)

    # b_dictonary = {}
    # with open("TemplateInterests.json", "r+") as file:
    #     data = json.load(file)
    #     for key in data:
    #         b_dictonary[key] = data[key]

    #print(b_dictonary)

    if request.method == 'POST':

        #print(request.form.get('newTemplatename'))
        #print(request.form.get('deleteTemplatename'))

        if str(request.form.get('newTemplatename')) != "":

            if request.form.get('submit') == "Submit":

                # with open("TemplateValues.json", "r+") as file:
                #     data = json.load(file)
                #     data["Templates"].append(request.form.get('newTemplatename'))
                #
                #     file.seek(0)
                #     json.dump(data, file)
                #     file.truncate()
                #
                # with open("TemplateInterests.json", "r+") as file:
                #     data = json.load(file)
                #     #tmp = data[templatevalue]
                #     data[request.form.get('newTemplatename')] = ""
                #     file.seek(0)
                #     json.dump(data, file)
                #     file.truncate()

                with open(file_name, "r+") as file:
                    data = json.load(file)

                    data["Reports"].append({
                        "reportname": "%s"%(request.form.get('newTemplatename')),
                        "intrestedtopics": "",
                        "visualization_template_option": ""
                    })

                    file.seek(0)
                    json.dump(data, file, indent=4)
                    file.truncate()

                return flask.render_template("page_14.html")

        if str(request.form.get('deleteTemplatename')) != "":

            if request.form.get('submit') == "Submit":

                # with open("TemplateValues.json", "r+") as file:
                #     deletedata = json.load(file)
                #     if request.form.get('deleteTemplatename') in deletedata["Templates"]:
                #         deletedata["Templates"].remove(request.form.get('deleteTemplatename'))
                #
                #         file.seek(0)
                #         json.dump(deletedata, file)
                #         file.truncate()
                #
                # with open("TemplateInterests.json", "r+") as file:
                #     deleteinterests = json.load(file)
                #     if request.form.get('deleteTemplatename') in deleteinterests:
                #         del deleteinterests[request.form.get('deleteTemplatename')]
                #
                #         file.seek(0)
                #         json.dump(deleteinterests, file)
                #         file.truncate()

                with open(file_name, "r+") as file:
                    data = json.load(file)
                    #print(data["Reports"])

                    for i in range(len(data["Reports"])):
                        if data["Reports"][i]["reportname"] == request.form.get('deleteTemplatename'):
                            data["Reports"].pop(i)
                            break

                    file.seek(0)
                    json.dump(data, file, indent=4)
                    file.truncate()

                return flask.render_template("page_14.html")

        # print(request.form.get("topicsofinterest"))
        # print(topicsOfInterest)
        #global templatevalue

        # for template in templatesall:
        #     if request.form.get(template) == template:
        #
        #         with open("TemplateValues.json", "r+") as file:
        #             data = json.load(file)
        #             data["Edittemplatevalue"] = template
        #
        #             file.seek(0)
        #             json.dump(data, file)
        #             file.truncate()
        #
        #         # with open("TemplateValues.json", "r+") as file:
        #         #     data = json.load(file)
        #         #     templatevalue = data["Edittemplatevalue"]
        #
        #         #templatevalue = template
        #         return flask.render_template("page_13.html", checkboxes=topicsOfInterest, templatevalue=template, \
        #                                      checked=b_dictonary[template], route="template1")

        #elif request.form.get("")

    return flask.render_template("page_22.html", form=form, templates=all_report_names, route="edit-templates-new")


@app.route('/edit-templates', methods=['GET', 'POST'])
def edit_templates():
    form = NewTemplateForm()

    # with open("TemplateValues.json", "r+") as file:
    #     data = json.load(file)
    #     templatesall = data["Templates"]

    # topicsOfInterest = []
    #
    # with open("query.json", "r+") as file:
    #     data = json.load(file)
    #     querydataintemplates = data
    #     for toi in data:
    #         if toi != 'null' and toi != '':
    #             topicsOfInterest.append(toi)

    #print(topicsOfInterest)

    global queryfile_name
    mantopicsall = []
    with open(queryfile_name, "r+") as manfile:
        mandata = json.load(manfile)

        for m in mandata["Topics_and_Queries"]:
            mantopicsall.append(m["topicname"])

    print(mantopicsall)

    global file_name
    report_names = []
    topics_of_interests = {}

    with open(file_name, "r+") as file:
        enduserreports = json.load(file)

        for report in enduserreports["Reports"]:
            report_names.append(report["reportname"])
            topics_of_interests[report["reportname"]] = report["intrestedtopics"]

    #print(report_names)
    #print(topics_of_interests)

    if request.method == 'POST':

        for report in report_names:
            if request.form.get(report) == report:
                session["Edittemplatevalue"] = report

                with open('sampledata.txt', 'r+') as outfile:
                    data = json.load(outfile)
                    finalzipdict = data['resultslatest']

                    checkedvalue = True

                return flask.render_template("page_13.html", checkboxes=mantopicsall, resultsupdated=finalzipdict,
                                             checkedtopics=checkedvalue, templatevalue=report,
                                             checked=topics_of_interests[report], route="template1")

    return flask.render_template("page_12.html", form=form, templates=report_names, route="edit-templates")


def returnresults(self, querylistforresults):
    #print(querylistforresults)
    resultsfoundnew = {}
    resultsfound = []

    for key in querylistforresults:
        querynew = querylistforresults[key]
        #print("Here New Query")
        #print(querynew)

        try:
            GeoRDFStore._do_query(querynew, querynew)
        except:
            print("Error While Executing Queries")
            return render_template("page1.html")

        for result in GeoRDFStore._do_query(querynew, querynew):
            newvar = {}
            # print(result.keys())
            # print(result.values())

            for i in range(len(result.keys())):
                # print(list(result.keys())[i])

                #print(list(result.values())[i]['value'])

                if "#" in list(result.values())[i]['value']:
                    newvar[list(result.keys())[i]] = list(result.values())[i]['value'].split('#')[1]
                else:
                    newvar[list(result.keys())[i]] = list(result.values())[i]['value']

            resultsfound.append(newvar)

        resultsfoundnew[key] = resultsfound

    #print(resultsfoundnew)

    results = resultsfoundnew
    resultsnew = list(results.keys())

    valuesupdaated = []
    for i in results:
        # print(results[i])
        m = {}
        for j in results[i]:
            # print(j)
            # print(list(j.keys()))
            mnew = list(j.keys())
            for s in range(len(list(j.keys()))):
                m[mnew[s]] = ""
                # print(m
            # print(m)
            break

        for j in results[i]:
            # print(mnew)
            for f in mnew:
                values = []
                # print("moff")
                # print(m[f])
                if m[f]:
                    m[f].append(j[f])
                else:
                    values.append(j[f])
                    m[f] = values

            # print("mmmmmm")
        valuesupdaated.append(m)
        # print("jjjjjjj")
        # print(j)

    #print(valuesupdaated)

    templist = []
    for one in valuesupdaated:
        newdict = {}
        currentdict = {}
        # print(len(one.values()))
        if len(one.values()) == 1:
            newdict[list(one.keys())[0]] = list(one.values())[0]
            currentdict["example"] = newdict
            templist.append(currentdict)

        if len(one.values()) == 2:
            anotherdict = {}
            count = 0
            for i in list(one.values()):
                list1 = i
                for m in i:
                    anotherdict[m] = ""

                break

            # print("list1")
            # print(list1)

            for s in list(one.values()):
                if count == 0:
                    count += 1
                    pass
                else:
                    list2 = s

            # print("list2")
            # print(list2)

            # zipbObj = zip(list1, list2)
            # print("zipobj")
            # dictOfWords = dict(zipbObj)
            # print(dictOfWords)
            # templist.append(dictOfWords)

            # print(list1)
            # print(list2)

            if 'image' in list1:
                size = len(list1)
                idx_list = [idx + 1 for idx, val in enumerate(list1) if val == 'image']
                res = [list1[i: j] for i, j in zip([0] + idx_list, idx_list + ([size] if idx_list[-1] != size else []))]
                # print(res)

                newresult5 = []
                lastcount5 = 0
                count5 = 0
                predobject = {}

                for i in res:
                    # print("i")
                    # print(i)
                    if count5 == 0:
                        # print(list2[:len(i)])
                        somevar5 = list2[:len(i)]
                    else:
                        newlength5 = lastcount5 + len(i)
                        # print(lastcount)
                        # print(newlength)
                        # print("Secondlist")
                        # print(list2[lastcount5:newlength5])
                        somevar5 = list2[lastcount5:newlength5]

                    # print(somevar5)
                    lastcount5 = lastcount5 + len(i)
                    predobject[count5] = dict(zip(i, somevar5))
                    count5 += 1
                    # newresult5.append(somevar5)

                # print("newresult5")
                # print(newresult5)
                # print(predobject)
                templist.append(predobject)
                # print(predobject)

            else:
                # list1updated = []
                # for item in list1:
                #     if item not in
                #     list1updated.append(item)
                count = 0
                samplelistlatest = {}

                dict2 = {}
                for key in list1:
                    dict1 = {}
                    for value in list2:
                        dict1[key] = value
                        list2.remove(value)
                        break

                    count += 1
                    dict2[count] = dict1

                templist.append(dict2)
                # print(dict2)

        othervalues = []
        count = 0
        journal = []
        if len(one.values()) == 3:
            # print("three values")
            for i in list(one.values()):
                # print(i)
                if count == 0:
                    for logs in i:
                        if logs not in journal:
                            journal.append(logs)
                    count += 1
                    # journal = list(set(i))
                    # print(journal)
                else:
                    # print("else loop")
                    # print(i)
                    othervalues.append(i)

            # print("Othervalues")
            # print(othervalues)
            # for i in othervalues:
            #     print("othervalues")
            #     print(i)
            #     print(len(i))

            if 'image' in othervalues[0]:
                size = len(othervalues[0])
                idx_list = [idx + 1 for idx, val in enumerate(othervalues[0]) if val == 'image']
                res = [othervalues[0][i: j] for i, j in
                       zip([0] + idx_list, idx_list + ([size] if idx_list[-1] != size else []))]
                # print(res)

                newresult = []
                lastcount = 0
                count = 0

                # print(othervalues[1][0:13])
                # print(othervalues[1][13:26])
                # print(othervalues[1][26:36])

                newlength = 0
                for i in res:
                    # print(len(i))
                    if count == 0:
                        somevar = othervalues[1][:len(i)]
                    else:
                        newlength = lastcount + len(i)
                        # print(lastcount)
                        # print(newlength)
                        somevar = othervalues[1][lastcount:newlength]

                    lastcount = lastcount + len(i)
                    count += 1
                    newresult.append(somevar)

                # print(newresult)

                journaldict = {}
                for numberedlist in range(len(res)):
                    zip1 = dict(zip(res[numberedlist], newresult[numberedlist]))
                    journaldict[journal[numberedlist]] = zip1

                templist.append(journaldict)
            else:
                # print(journal)
                # print(othervalues)
                dictone = {}

                for numberedlist in range(len(journal)):
                    for key in othervalues[0]:
                        dicttwo = {}
                        for value in othervalues[1]:
                            dicttwo[key] = value
                            othervalues[1].remove(value)
                            # print(dicttwo)
                            break

                        try:
                            dictone[journal[numberedlist]] = dicttwo
                        except:
                            print("Error here")

                        break

                templist.append(dictone)

        # print(othervalues[0])

    # print(templist)
    # print(newdict)

    # zip1 = dict(zip(othervalues[0], othervalues[1]))
    # journaldict = {}
    # if len(journal) == 1:
    #     journaldict[journal[0]] = zip1

    # print(journaldict)
    # templist.append(journaldict)
    # car_data = dict(zip(journal, zip(othervalues[0], othervalues[1])))
    # print(car_data)

    finalzip = zip(resultsnew, templist)
    finalzipdict = dict(finalzip)

    #print("finaldict")
    #print(finalzipdict)

    finaldict = {}
    x = list(results.keys())
    for l in range(len(x)):
        finaldict[x[l]] = valuesupdaated[l]

    #print("finaldict")

    return finalzipdict


@app.route('/template1', methods=['GET', 'POST'])
def template1():
    #print("entering")

    # topicsOfInterest = []
    # with open("query.json", "r+") as file:
    #     data = json.load(file)
    #     for toi in data:
    #         if toi != 'null' and toi != '':
    #             topicsOfInterest.append(toi)

    if request.method == 'POST':
        #print(request.form.getlist("topicsofinterest"))
        #print(request.form.get("visualization1"))

        global file_name

        with open(file_name, "r+") as file:
            data = json.load(file)
            print(session["Edittemplatevalue"])

            for report in data["Reports"]:
                if report["reportname"] == session["Edittemplatevalue"]:
                    report["intrestedtopics"] = request.form.getlist("topicsofinterest")
                    report["visualization_template_option"] = request.form.get("visualization1")

                    file.seek(0)
                    json.dump(data, file, indent=4)
                    file.truncate()

        return flask.render_template("page_14.html")

    return flask.render_template("page_14.html")


@app.route('/page3', methods=['GET', 'POST'])
def choose_activity():
    routing = ''
    if request.method == 'POST':
        if request.form.get('fielddetails') == 'Field Details':
            routing = "page4"
            return redirect(url_for('activity_form_details'))
        elif request.form.get('seedingdetails') == 'Seeding':
            routing = "page4"
            return redirect(url_for('activity_form_details'))
        elif request.form.get('plantationdetails') == 'Plantation Details':
            routing = "page4"
            return redirect(url_for('activity_form_details'))
        elif request.form.get('harvesting details') == 'Harvesting Details':
            routing = "page4"
            return redirect(url_for('activity_form_details'))
        elif request.form.get('wateringdetails') == 'Watering Details':
            routing = "page4"
            return redirect(url_for('activity_form_details'))
        elif request.form.get('submit') == 'Submit':
            return redirect(url_for('activity_form_details'))

    return render_template("page3.html", route="%s"%(routing))

@app.route('/page4', methods=['GET', 'POST'])
def activity_form_details():
    routing = ""
    if request.method == 'POST':
        if request.form.get('Submit') == 'Submit':
            return redirect(url_for('edit_activity_details'))

    return render_template("page4.html", route="page5")

@app.route('/page5', methods=['GET', 'POST'])
def edit_activity_details():
    routing = ''
    if request.method == 'POST':
        if request.form.get('Edit') == 'Edit':
            routing = "page4"
            return redirect(url_for('activity_form_details'))
        elif request.form.get('Submit') == 'Submit':
            routing = "/"
            return redirect(url_for('hello_world'))

    return render_template("page5.html", route="%s"%(routing))

@app.route('/page6', methods=['GET', 'POST'])
def choose_end_user():
    # routing = ""
    # global enduser
    # if request.method == 'POST':
    #     if request.form.get('Agriculture Minister') == 'Agriculture Minister':
    #         enduser = "agm"
    #         routing = "templates"
    #         return redirect(url_for('choosetemplates'))
    #     elif request.form.get('Contractor') == 'Contractor':
    #         enduser = "contractor"
    #         routing = "page7"
    #         return redirect(url_for('choosetemplates'))
    #     elif request.form.get('Customer') == 'Customer':
    #         enduser = "customer"
    #         routing = "page7"
    #         return redirect(url_for('choosetemplates'))
    #     # elif request.form.get('Custom Query') == 'Custom Query':
    #     #     return redirect(url_for('customquerynew'))

    # {"Templates": ["AgricultureMinister", "Contractor", "Customer"]}

    global file_name
    enduser_reportnames = []

    with open(file_name, "r+") as file:
        data = json.load(file)

        for report in data["Reports"]:
            enduser_reportnames.append(report["reportname"])

    if request.method == "POST":
        #print(request.form.get("Contractor"))
        for report in enduser_reportnames:
            #print(report)
            #print("hello")
            #print(request.form.get(report))
            if request.form.get(report) == report:
                #print("entered")
                #print(report)
                session['Enduser'] = report
            else:
                continue
            #print(session['Enduser'] )
            return redirect(url_for("templateresult_display"))


    # with open("TemplateValues.json", "r+") as file:
    #     data = json.load(file)
    #     templatesall = data["Templates"]
    #
    # if request.method == "POST":
    #     for template in templatesall:
    #         if request.form.get(template) == template:
    #             session['enduser'] = template
    #             with open("TemplateValues.json", "r+") as file:
    #                 data = json.load(file)
    #                 data["PotentialEndUser"] = template
    #                 file.seek(0)
    #                 json.dump(data, file)
    #                 file.truncate()
    #
    #             return redirect(url_for("templateresult_display"))

    return render_template("page6.html", templates=enduser_reportnames, route="page6")


@app.route('/templateresult_display', methods=['GET', 'POST'])
def templateresult_display():

    form1 = QuerySelctionForm()
    form2 = QuerySelctionForm1()

    if not form1.validate_on_submit() and not form2.validate_on_submit():
        return render_template("page_15.html", form=form1, route="templateresult_display")

    if form1.validate_on_submit():
        #print(form.wkt.data)
        ### WKT QUERY
        geojson = GeoRDFStore.wkt2geojson(form1.wkt.data)
        #print(form1.dateFrom.data)
        #print(form1.dateTo.data)
        datestring = "(?date >= xsd:date(%s) && ?date <= xsd:date(%s))"%(form1.dateFrom.data, form1.dateTo.data)
        #print(geojson)
        results = STORE.query_wkt(form1.wkt.data)
        print(results)

        print(form1.dateFrom.data)
        print(form1.dateTo.data)

        dateresults = STORE.query_date(form1.dateFrom.data, form1.dateTo.data)

        print("dateresults")
        print(dateresults)

        #print(results)
        #print(dateresults)

        updatedresults = []
        for result in results:
            if result in dateresults:
                updatedresults.append(result)
            else:
                continue

        print("Updatedresults")
        print(updatedresults)

        resultsfound = []
        resultsfoundnew = {}

        #detailscustomquery = [STORE.get_details(_, ) for _ in results]
        #print(detailscustomquery)

        #### Query string for log journals
        samplestring = ""
        for logjournal in updatedresults:
            samplestring += "?subject = <%s> || " % logjournal

        samplestring = samplestring[:-3]

        # if request.method == "POST":
        #     outputvalue = flask.request.form.get('HTML')
        #     print(outputvalue)
        #
        #     print("post request")
        #     if flask.request.form.get("Submit") == "Submit":
        #         print("pressed submit")
        #         outputvalue = request.form.get('example')
        #         print(outputvalue)

        # with open("TemplateValues.json", "r+") as file:
        #     data = json.load(file)
        #     templatevalue = data["PotentialEndUser"]

        ### Get the Predefined Query For The User
        # with open("TemplateInterests.json", "r+") as file:
        #     data = json.load(file)
        #     needtoquery = data[templatevalue]

        global file_name

        with open(file_name, "r+") as file:
            data = json.load(file)

            for report in data["Reports"]:
                if report["reportname"] == session["Enduser"]:
                    needtoquery = report["intrestedtopics"]
                    parameter = report["visualization_template_option"]

        #print(session["Enduser"])
        #print("NeedToQuery")
        #print(needtoquery)
        #print(parameter)
        session["parameter"] = parameter

        global queryfile_name

        print(needtoquery)
        resultsfortemplate = {}

        with open(queryfile_name, "r+") as omfile:
            omdata = json.load(omfile)

            for i in range(len(omdata["Topics_and_Queries"])):
                if omdata["Topics_and_Queries"][i]["topicname"] in needtoquery:
                    resultsfortemplate[omdata["Topics_and_Queries"][i]["topicname"]] = omdata["Topics_and_Queries"][i]["queryvalue"]
                else:
                    continue

        #for query in needtoquery:
        #    for i in range(len(omdata["Topics_and_Queries"])):


        #    resultsfortemplate[query] = query_latest

            # with open("query.json", "r+") as file:
            #     datalatest = json.load(file)
            #     query_latest = datalatest[query]
            #     #print(query_latest)
            #
            # resultsfortemplate[query] = query_latest

        print(resultsfortemplate)
        print(needtoquery)

        for topic in needtoquery:
            if topic != "":
                resultsfoundnew[topic] = ""

        #print(resultsfoundnew[topic])

        if not bool(resultsfortemplate):
            message = "Sorry....No Details Found, The Queries Are Not Assigned To The EndUser Templates"
            return render_template("page_15.html", form=form1, message=message, route="templateresult_display")

        numberinit = 0
        for query in resultsfortemplate.values():
            if resultsfound:
                resultsfoundnew[needtoquery[numberinit]] = resultsfound
                numberinit += 1
                resultsfound = []

            #print("evaluate")
            querynew = ""

            if updatedresults:
                if "Location Filter" in query:
                    print("entered here")
                    querynew = query.replace("**** Location Filter ****", "?subject ?predicate ?object FILTER (%s) ." % (samplestring))

                # else:
                #     temp = str(query.splitlines()[9])
                #     temp3 = ""
                #     temp2 = ""
                #
                #     if "FILTER" not in temp and "filter" not in temp:
                #         temp3 = temp.replace(".", " ")
                #         temp2 = temp3 + " FILTER (%s) ." % (samplestring)
                #         querynew = query.replace(temp, temp2)
                #
                #     if "FILTER" in temp or "filter" in temp:
                #         if "))" in temp:
                #             temp2 = temp.replace("))", ") && (%s))"%samplestring)
                #         elif ") )" in temp:
                #             temp2 = temp.replace(") )", ") && (%s) )" % samplestring)
                #
                #         querynew = query.replace(temp, temp2)

            else:
                message = "Sorry....No Details Found, Please Choose Correct Area In The Maps And DateField"
                return render_template("page_15.html", form=form1, message=message, route="templateresult_display")

            #temp3 = temp.replace(".", " ")
            #querynew = query.replace("?subject ?predicate ?object .", "?subject ?predicate ?object FILTER ( str(?subject) = \"%s\" ) ." % results[0])
            #temp2 = temp3 + " FILTER (%s) ."%(samplestring)
            #querynew = query.replace(temp, temp2)

            #print("querynew")
            #print(querynew)

            try:
                GeoRDFStore._do_query(querynew, querynew)
            except:
                message = "Sorry....No Details Found, Please Choose Another Area In The Maps"
                return render_template("page_15.html", form=form1, message=message, route="templateresult_display")


            print(querynew)
            for result in GeoRDFStore._do_query(querynew, querynew):
                #print(result)
                newvar = {}
                # print(result.keys())
                # print(result.values())

                for i in range(len(result.keys())):
                    # print(list(result.keys())[i])

                    #print(list(result.values())[i]['value'])

                    if "#" in list(result.values())[i]['value']:
                        newvar[list(result.keys())[i]] = list(result.values())[i]['value'].split('#')[1]
                    else:
                        newvar[list(result.keys())[i]] = list(result.values())[i]['value']

                resultsfound.append(newvar)

        print(resultsfound)
        resultsfoundnew[needtoquery[numberinit]] = resultsfound
        #session['my_var'] = resultsfoundnew

        data = {}
        with open('data.txt', 'w') as outfile:
            data['resultslatest'] = resultsfoundnew
            json.dump(data, outfile, indent=4)

        #print(resultsfoundnew)

        #results = data['resultslatest']
        # print(results)

        results = resultsfoundnew
        resultsnew = list(results.keys())
        # print(resultsnew)

        valuesupdaated = []
        for i in results:
            # print(results[i])
            m = {}
            for j in results[i]:
                # print(j)
                # print(list(j.keys()))
                mnew = list(j.keys())
                for s in range(len(list(j.keys()))):
                    m[mnew[s]] = ""
                    # print(m
                # print(m)
                break

            for j in results[i]:
                # print(mnew)
                for f in mnew:
                    values = []
                    # print("moff")
                    # print(m[f])
                    if m[f]:
                        m[f].append(j[f])
                    else:
                        values.append(j[f])
                        m[f] = values

                # print("mmmmmm")
            valuesupdaated.append(m)
            # print("jjjjjjj")
            # print(j)

        #print(valuesupdaated)

        templist = []
        for one in valuesupdaated:
            newdict = {}
            currentdict = {}
            # print(len(one.values()))
            if len(one.values()) == 1:
                newdict[list(one.keys())[0]] = list(one.values())[0]
                currentdict["example"] = newdict
                templist.append(currentdict)

            if len(one.values()) == 2:
                anotherdict = {}
                count = 0
                for i in list(one.values()):
                    list1 = i
                    for m in i:
                        anotherdict[m] = ""

                    break

                # print("list1")
                # print(list1)

                for s in list(one.values()):
                    if count == 0:
                        count += 1
                        pass
                    else:
                        list2 = s

                # print("list2")
                # print(list2)

                # zipbObj = zip(list1, list2)
                # print("zipobj")
                # dictOfWords = dict(zipbObj)
                # print(dictOfWords)
                # templist.append(dictOfWords)

                # print(list1)
                # print(list2)

                if 'maxy' in list1:
                    size = len(list1)
                    idx_list = [idx + 1 for idx, val in enumerate(list1) if val == 'maxy']
                    res = [list1[i: j] for i, j in
                           zip([0] + idx_list, idx_list + ([size] if idx_list[-1] != size else []))]
                    # print(res)

                    newresult5 = []
                    lastcount5 = 0
                    count5 = 0
                    predobject = {}

                    for i in res:
                        # print("i")
                        # print(i)
                        if count5 == 0:
                            # print(list2[:len(i)])
                            somevar5 = list2[:len(i)]
                        else:
                            newlength5 = lastcount5 + len(i)
                            # print(lastcount)
                            # print(newlength)
                            # print("Secondlist")
                            # print(list2[lastcount5:newlength5])
                            somevar5 = list2[lastcount5:newlength5]

                        # print(somevar5)
                        lastcount5 = lastcount5 + len(i)
                        predobject[count5] = dict(zip(i, somevar5))
                        count5 += 1
                        # newresult5.append(somevar5)

                    # print("newresult5")
                    # print(newresult5)
                    # print(predobject)
                    templist.append(predobject)
                    # print(predobject)

                else:
                    # list1updated = []
                    # for item in list1:
                    #     if item not in
                    #     list1updated.append(item)
                    count = 0
                    samplelistlatest = {}

                    dict2 = {}
                    for key in list1:
                        dict1 = {}
                        for value in list2:
                            dict1[key] = value
                            list2.remove(value)
                            break

                        count += 1
                        dict2[count] = dict1

                    templist.append(dict2)
                    # print(dict2)

            othervalues = []
            count = 0
            journal = []
            if len(one.values()) == 3:
                # print("three values")
                for i in list(one.values()):
                    # print(i)
                    if count == 0:
                        for logs in i:
                            if logs not in journal:
                                journal.append(logs)
                        count += 1
                        # journal = list(set(i))
                        # print(journal)
                    else:
                        # print("else loop")
                        # print(i)
                        othervalues.append(i)

                # print("Othervalues")
                # print(othervalues)
                # for i in othervalues:
                #     print("othervalues")
                #     print(i)
                #     print(len(i))

                if 'maxy' in othervalues[0]:
                    size = len(othervalues[0])
                    idx_list = [idx + 1 for idx, val in enumerate(othervalues[0]) if val == 'maxy']
                    res = [othervalues[0][i: j] for i, j in
                           zip([0] + idx_list, idx_list + ([size] if idx_list[-1] != size else []))]
                    # print(res)

                    newresult = []
                    lastcount = 0
                    count = 0

                    # print(othervalues[1][0:13])
                    # print(othervalues[1][13:26])
                    # print(othervalues[1][26:36])

                    newlength = 0
                    for i in res:
                        # print(len(i))
                        if count == 0:
                            somevar = othervalues[1][:len(i)]
                        else:
                            newlength = lastcount + len(i)
                            # print(lastcount)
                            # print(newlength)
                            somevar = othervalues[1][lastcount:newlength]

                        lastcount = lastcount + len(i)
                        count += 1
                        newresult.append(somevar)

                    # print(newresult)

                    journaldict = {}
                    for numberedlist in range(len(res)):
                        zip1 = dict(zip(res[numberedlist], newresult[numberedlist]))
                        journaldict[journal[numberedlist]] = zip1

                    templist.append(journaldict)
                else:
                    # print(journal)
                    # print(othervalues)
                    dictone = {}

                    for numberedlist in range(len(journal)):
                        for key in othervalues[0]:
                            dicttwo = {}
                            for value in othervalues[1]:
                                dicttwo[key] = value
                                othervalues[1].remove(value)
                                # print(dicttwo)
                                break

                            try:
                                dictone[journal[numberedlist]] = dicttwo
                            except:
                                print("Error here")

                            break

                    templist.append(dictone)

            # print(othervalues[0])

        # print(templist)
        # print(newdict)

        # zip1 = dict(zip(othervalues[0], othervalues[1]))
        # journaldict = {}
        # if len(journal) == 1:
        #     journaldict[journal[0]] = zip1

        # print(journaldict)
        # templist.append(journaldict)
        # car_data = dict(zip(journal, zip(othervalues[0], othervalues[1])))
        # print(car_data)

        finalzip = zip(resultsnew, templist)
        finalzipdict = dict(finalzip)
        # print(finalzipdict)

        finaldict = {}
        x = list(results.keys())
        for l in range(len(x)):
            finaldict[x[l]] = valuesupdaated[l]

        #if request.method == "POST":

        # enduservalue = session.get('enduser')
        #print(enduservalue)
        # with open('Visualization.json') as outfile:
        #     datavisul = json.load(outfile)
        #     parameter = datavisul[enduservalue]

        #print(parameter)
        print(finalzipdict)

        if parameter == "Visualization1":
            return render_template("page_20.html", form=form2, farmvalue=True, resultsupdated=finalzipdict, route="templateresult_display")

        elif parameter == "Visualization2":
            return render_template("page_19.html", form=form2, farmvalue=True, resultsupdated=finalzipdict, route="templateresult_display")

        elif parameter == "Visualization3":
            return render_template("page_18.html", form=form2, farmvalue=True, resultsupdated=finalzipdict, route="templateresult_display")

        else:
            return render_template("page_16.html", form=form2, tripples=resultsfound, topicwithqueryvalues=resultsfoundnew, \
                           prefill=geojson, route="templateresult_display")

    if form2.validate_on_submit():
        print("form2 is pressed")
        with open('data.txt', 'r+') as outfile:
            data = json.load(outfile)

        results = data['resultslatest']
        #print(results)
        resultsnew = list(results.keys())
        #print(resultsnew)


        valuesupdaated = []
        for i in results:
            #print(results[i])
            m = {}
            for j in results[i]:
                #print(j)
                #print(list(j.keys()))
                mnew = list(j.keys())
                for s in range(len(list(j.keys()))):
                    m[mnew[s]] = ""
                    #print(m
                #print(m)
                break


            for j in results[i]:
                #print(mnew)
                for f in mnew:
                    values = []
                    #print("moff")
                    #print(m[f])
                    if m[f]:
                        m[f].append(j[f])
                    else:
                        values.append(j[f])
                        m[f] = values

                #print("mmmmmm")
            valuesupdaated.append(m)
                #print("jjjjjjj")
                #print(j)


        #print(valuesupdaated)

        templist = []
        for one in valuesupdaated:
            newdict = {}
            currentdict = {}
            #print(len(one.values()))
            if len(one.values()) == 1:
                newdict[list(one.keys())[0]] = list(one.values())[0]
                currentdict["example"] = newdict
                templist.append(currentdict)

            if len(one.values()) == 2:
                anotherdict = {}
                count = 0
                for i in list(one.values()):
                    list1 = i
                    for m in i:
                        anotherdict[m] = ""

                    break

                #print("list1")
                #print(list1)

                for s in list(one.values()):
                    if count == 0:
                        count += 1
                        pass
                    else:
                        list2 = s

                #print("list2")
                #print(list2)

                # zipbObj = zip(list1, list2)
                # print("zipobj")
                # dictOfWords = dict(zipbObj)
                # print(dictOfWords)
                # templist.append(dictOfWords)

                #print(list1)
                #print(list2)

                if 'maxy' in list1:
                    size = len(list1)
                    idx_list = [idx + 1 for idx, val in enumerate(list1) if val == 'maxy']
                    res = [list1[i: j] for i, j in zip([0] + idx_list, idx_list + ([size] if idx_list[-1] != size else []))]
                    #print(res)

                    newresult5 = []
                    lastcount5 = 0
                    count5 = 0
                    predobject = {}

                    for i in res:
                        #print("i")
                        #print(i)
                        if count5 == 0:
                            #print(list2[:len(i)])
                            somevar5 = list2[:len(i)]
                        else:
                            newlength5 = lastcount5 + len(i)
                            #print(lastcount)
                            #print(newlength)
                            #print("Secondlist")
                            #print(list2[lastcount5:newlength5])
                            somevar5 = list2[lastcount5:newlength5]

                        #print(somevar5)
                        lastcount5 = lastcount5 + len(i)
                        predobject[count5] = dict(zip(i, somevar5))
                        count5 += 1
                        #newresult5.append(somevar5)


                    #print("newresult5")
                    #print(newresult5)
                    #print(predobject)
                    templist.append(predobject)
                    #print(predobject)

                else:
                    # list1updated = []
                    # for item in list1:
                    #     if item not in
                    #     list1updated.append(item)
                    count = 0
                    samplelistlatest = {}

                    dict2 = {}
                    for key in list1:
                        dict1 = {}
                        for value in list2:
                            dict1[key] = value
                            list2.remove(value)
                            break

                        count += 1
                        dict2[count] = dict1

                    templist.append(dict2)
                    #print(dict2)

            othervalues = []
            count = 0
            journal =[]
            if len(one.values()) == 3:
                #print("three values")
                for i in list(one.values()):
                    #print(i)
                    if count == 0:
                        for logs in i:
                            if logs not in journal:
                                journal.append(logs)
                        count += 1
                        #journal = list(set(i))
                        #print(journal)
                    else:
                        #print("else loop")
                        #print(i)
                        othervalues.append(i)

                #print("Othervalues")
                #print(othervalues)
                # for i in othervalues:
                #     print("othervalues")
                #     print(i)
                #     print(len(i))

                if 'maxy' in othervalues[0]:
                    size = len(othervalues[0])
                    idx_list = [idx + 1 for idx, val in enumerate(othervalues[0]) if val == 'maxy']
                    res = [othervalues[0][i: j] for i, j in zip([0] + idx_list, idx_list + ([size] if idx_list[-1] != size else []))]
                    #print(res)

                    newresult = []
                    lastcount = 0
                    count = 0

                    # print(othervalues[1][0:13])
                    # print(othervalues[1][13:26])
                    # print(othervalues[1][26:36])

                    newlength = 0
                    for i in res:
                        #print(len(i))
                        if count == 0:
                            somevar = othervalues[1][:len(i)]
                        else:
                            newlength = lastcount + len(i)
                            #print(lastcount)
                            #print(newlength)
                            somevar = othervalues[1][lastcount:newlength]

                        lastcount = lastcount + len(i)
                        count += 1
                        newresult.append(somevar)

                    #print(newresult)

                    journaldict = {}
                    for numberedlist in range(len(res)):
                        zip1 = dict(zip(res[numberedlist], newresult[numberedlist]))
                        journaldict[journal[numberedlist]] = zip1

                    templist.append(journaldict)
                else:
                    #print(journal)
                    #print(othervalues)
                    dictone = {}

                    for numberedlist in range(len(journal)):
                        for key in othervalues[0]:
                            dicttwo = {}
                            for value in othervalues[1]:
                                dicttwo[key] = value
                                othervalues[1].remove(value)
                                #print(dicttwo)
                                break

                            try:
                                dictone[journal[numberedlist]] = dicttwo
                            except:
                                print("Error here")

                            break

                    templist.append(dictone)

            #print(othervalues[0])


        #print(templist)
        #print(newdict)

        # zip1 = dict(zip(othervalues[0], othervalues[1]))
        # journaldict = {}
        # if len(journal) == 1:
        #     journaldict[journal[0]] = zip1

        # print(journaldict)
        # templist.append(journaldict)
        #car_data = dict(zip(journal, zip(othervalues[0], othervalues[1])))
        #print(car_data)

        finalzip = zip(resultsnew, templist)
        finalzipdict = dict(finalzip)
        #print(finalzipdict)

        finaldict = {}
        x = list(results.keys())
        for l in range(len(x)):
            finaldict[x[l]] = valuesupdaated[l]

        #print(finaldict)

        data = {}
        with open('data2.txt', 'w') as outfile:
            data['resultslatest'] = finalzipdict
            json.dump(data, outfile, indent=4)

        # enduservalue = session.get('enduser')
        # print(enduservalue)
        # with open('Visualization.json') as outfile:
        #     datavisul = json.load(outfile)
        #     parameter = datavisul[enduservalue]

        parameter = session["parameter"]
        print(parameter)
        form = ""

        if form2.language.data == "pdf":
            if parameter == "Visualization1":
                htmlelement = flask.render_template("template1.html",  farmvalue=False, resultsupdated=finalzipdict)
                return render_pdf(HTML(string=htmlelement))

            elif parameter == "Visualization2":
                htmlelement = flask.render_template("template2.html",  farmvalue=False, resultsupdated=finalzipdict)
                return render_pdf(HTML(string=htmlelement))

            elif parameter == "Visualization3":
                htmlelement = flask.render_template("template3.html",  farmvalue=False, resultsupdated=finalzipdict)
                return render_pdf(HTML(string=htmlelement))

                #htmlelement = flask.render_template("page_17.html", resulttodisplay=results)
                #return render_pdf(HTML(string=htmlelement))

            # htmlelement = flask.render_template("page_17.html", resulttodisplay=results)
            # pdfkit.from_file("templates/page_17.html", "output.pdf")

            #response = make_response(pdf)
            #response.headers['Content-Type'] = "application/pdf"
            #response.headers['Content-Disposition'] = 'inline; filename=output.pdf'

            #
            # return render_pdf(HTML(string=htmlelement))
            #return render_template("page_17.html", resulttodisplay=results)

        if form2.language.data == "html":
            print("html here")
            #print(session.get('my_var', None))
            if parameter == "Visualization1":
                return flask.render_template("page_20.html", farmvalue=False, resultsupdated=finalzipdict)

            elif parameter == "Visualization2":
                return flask.render_template("page_19.html", farmvalue=False, resultsupdated=finalzipdict)

            elif parameter == "Visualization3":
                return flask.render_template("page_18.html", farmvalue=False, resultsupdated=finalzipdict)

            return render_template("page_17.html", resulttodisplay=results)

        if form2.language.data == "latex":
            #return render_template("page_18.html", finalresult=finaldict, resultsupdated=finalzipdict)
            #return render_template("page_19.html", resultsupdated=finalzipdict)
            return render_template("page_20.html", resultsupdated=finalzipdict)


@app.route('/page7', methods=['GET', 'POST'])
def choose_end_user_location():
    if request.method == 'POST':
        if request.form.get('Submit') == 'submit':
            return redirect(url_for('view_report'))

    return render_template("page7.html", route="page8")

@app.route("/choosetemplates", methods=["GET", "POST"])
def choosetemplates():
    form = QuerySelctionForm()

    #print("1111111")
    #print(form.wkt.data)

    if not form.validate_on_submit():
        # geojson = GeoRDFStore.wkt2geojson(form.wkt.data)
        # print(geojson)

        # print("I pressed submit")
        #print("22222222222")
        return flask.render_template("page_8.html", form=form, route="choosetemplates")

    topicsToQuery = flask.request.form.get('topicsOfInterests')
    results = STORE.query_wkt(form.wkt.data)
    #print("333333333")
    global wktdata
    wktdata = form.wkt.data
    geojson = GeoRDFStore.wkt2geojson(form.wkt.data)

    if flask.request.form.get('two') == "Custom Query":
        return redirect(url_for('customquery'))

    if flask.request.form.get('three') == "View Specific Activity On Field":
        return redirect(url_for('templates'))

    if flask.request.form.get('four') == "View Reports":
        im = pyautogui.screenshot(region=(60, 180, 1800, 500))
        global file_path
        file_path = "static/images/imagefile%s.png"%(time.strftime("%Y%m%d-%H%M%S"))
        im.save(file_path)
        return redirect(url_for('customreports'))


@app.route("/customquerynew", methods=["GET", "POST"])
def customquerynew():
    form = CustomQueryForm()

    if not form.validate_on_submit():
        #print("Before pressing Submit")
        return flask.render_template("page_11.html", form=form, results="", selectoption="none", route="customquerynew")

    ### WKT Query
    geojson = GeoRDFStore.wkt2geojson(form.wkt.data)
    results = STORE.query_wkt(form.wkt.data)
    #print(results)

    #### Query string for log journals
    samplestring = ""
    for logjournal in results:
        samplestring += "?subject = <%s> || " % logjournal

    samplestring = samplestring[:-3]

    query_from_user = form.querryfield.data
    tempFound = []
    error = False

    print("query from user")
    print(query_from_user)

    #try:
    #    GeoRDFStore._do_query(query_from_user, query_from_user)
    #except:
    #    error = True
    #    return flask.render_template("page1.html")

    if query_from_user:
        # for result in results:
        #     #print(result)

        query_from_userentered = form.querryfield.data

        if "Location Filter" in query_from_userentered and samplestring != '':
            querynew = query_from_userentered.replace("**** Location Filter ****",  \
                                                                "?subject ?predicate ?object FILTER (%s) ." % (samplestring))

            print("new query")
            print(querynew)

        ### OLD CUSTOM QUERY FILTER ###

        # else:
        #     temp = str(query_from_userentered.splitlines()[9])
        #
        #     if "FILTER" not in temp:
        #         temp3 = temp.replace(".", " ")
        #         temp2 = temp3 + " FILTER (%s) ." % (samplestring)
        #         querynew = query_from_userentered.replace(temp, temp2)
        #     elif "FILTER" in temp:
        #         if ") )" in temp:
        #             temp2 = temp.replace(") )", ") && (%s) )"%samplestring)
        #         elif "))" in temp:
        #             temp2 = temp.replace("))", ") && (%s) )" % samplestring)
        #
        #         querynew = query_from_userentered.replace(temp, temp2)

        # if "FILTER" is not in query_from_userentered:
        #     query_from_user = query_from_userentered.replace("?subject ?predicate ?object .", "?subject ?predicate ?object FILTER ( str(?subject) = \"%s\" ) ."%result)
        #print(query_from_user)
        #query_latest = query5 % query_from_user

            for result in GeoRDFStore._do_query(querynew, querynew):
                newvar = {}
                #print(result.keys())
                #print(result.values())

                print("result")
                print(result)

                if not result.keys():
                    detailscustomquery = [{"Error": "Empty Results Returned.....Please check the Query again"}]

                    return flask.render_template("page_4.html", form=form, prefill=geojson, selectoption="none",
                                                 results=detailscustomquery, allgood=False,
                                                 route="customquerynew")

                for i in range(len(result.keys())):
                    #print(list(result.keys())[i])

                    newvar[list(result.keys())[i]] = list(result.values())[i]['value']


                    # newvar = {'%s': '', '%s': '', '%s': ''}%()
                    # try:
                    #     newvar['subject'] = result['subject']['value']
                    #     #tempFound.append(result['subject']['value'])
                    #     subjectvalue = True
                    # except:
                    #     print("no Subject value")
                    #
                    # try:
                    #     newvar['predicate'] = result['predicate']['value']
                    #     #tempFound.append(result['predicate']['value'])
                    #     predicatevalue = True
                    # except:
                    #     print("no predicate value")
                    #
                    # try:
                    #     newvar['object'] = result['object']['value']
                    #     #tempFound.append(result['object']['value'])
                    #     objectvalue = True
                    # except:
                    #     print("no object value")

                #print(newvar)
                tempFound.append(newvar)

                #print(tempFound)

                # newresults = []
                # for i in range(len(results)):
                #     if results[i] in tempFound and results[i] not in newresults:
                #         #rint("yes")
                #         newresults.append(results[i])
                #     else:
                #         continue
                #         # print("no")

                #print(newresults)

            detailscustomquery = [STORE.get_details1(_, ) for _ in results]

            if detailscustomquery == []:
                tripples = [{"Error": "Empty Results Returned.....Please check the Query again"}]

                return flask.render_template("page_4.html", form=form, prefill=geojson, selectoption="none",
                                             tripples=detailscustomquery, allgood=False,
                                             route="customquerynew")
            else:
                topicsofinterest = request.form.get('topicsofinterest')

                global queryfile_name

                if os.path.exists(queryfile_name):
                    with open(queryfile_name, "r+") as query_file_one:
                        query_data_json = json.load(query_file_one)

                        #print(topicsofinterest)

                        for d in query_data_json["Topics_and_Queries"]:
                            if topicsofinterest == d["topicname"]:
                                topicError = [{"Error": "Please Choose Another Topicname....Already this topic name is assigned to Another Query"}]

                                return flask.render_template("page_4.html", form=form, prefill=geojson,
                                                             selectoption="none", tripples=topicError, allgood=False,
                                                             route="customquerynew")

                            else:
                                continue

                        if topicsofinterest is not None:
                            #print("yes entering")
                            query_data_json["Topics_and_Queries"].append({
                                "topicname": "%s"%(topicsofinterest),
                                "queryvalue": "%s"%(query_from_userentered)
                            })

                            query_file_one.seek(0)
                            json.dump(query_data_json, query_file_one, indent=4)
                            query_file_one.truncate()

                        # reportobject = data["Reports"]
                        # for report in reportobject:
                        #     all_report_names.append(report["reportname"])
                else:
                    initialdata = {}
                    initialdata["Topics_and_Queries"] = []
                    initialdata["Topics_and_Queries"].append({
                        "topicname": "sample1",
                        "queryvalue": "sample2"
                    })

                    with open(queryfile_name, "w") as query_file_two:
                        json.dump(initialdata, query_file_two, indent=4)

                # with open("query.json", "r+") as topicfile:
                #     datafile = json.load(topicfile)
                #     if topicsofinterest in datafile:
                #
                #         print("same topic name")
                #         topicError = [{"Error": "Please Choose Another Topicname"}]
                #
                #         return flask.render_template("page_4.html", form=form, prefill=geojson, selectoption="none",
                #                                      tripples=topicError, allgood=False,
                #                                      route="customquerynew")
                #
                #     else:
                #         a_dictionary = {topicsofinterest: query_from_userentered}
                #
                #         datafile.update(a_dictionary)
                #
                #         topicfile.seek(0)
                #         json.dump(datafile, topicfile, indent=4)
                #         topicfile.truncate()

                if request.method == 'POST':
                    print(topicsofinterest)
                    if topicsofinterest is not None:
                        if request.form.get("submit") == "Submit":
                            return flask.render_template("page_14.html")

        # topicsofinterestDictionary.update({topicsofinterest:query_latest})

        #form2 = OutputSelectionForm()
        # global htmlelement

        # if selectcheck == "1":
        #     htmlelement = flask.render_template("page_6.html", details=detailscustomquery, route="reports")
        #return flask.render_template("page_5.html", form=form2, details=detailscustomquery, route="reports")

        # outputselection = request.args.get('outputselection')
        #print(outputselection)

            return flask.render_template("page_4.html", form=form, prefill=geojson, selectoption="block", tripples=tempFound, allgood=True, route="customquerynew")

        else:
            detailscustomquery = [{"Error": "Something Wrong With Query......Please check and submit again"}]

            return flask.render_template("page_4.html", form=form, prefill=geojson, selectoption="none",
                                         tripples=detailscustomquery, allgood=False,
                                         route="customquerynew")

    else:
        detailscustomquery = [{"Error": "Something Wrong With Query......Please check and submit again"}]

        return flask.render_template("page_4.html", form=form, prefill=geojson, selectoption="none", tripples=detailscustomquery,
                                     allgood=False, route="customquerynew")


@app.route("/reports", methods=["GET", "POST"])
def reports():
    reportformat = flask.request.form.get('outputselection')
    #checkbox = flask.request.form.get('checkbox')

    #print(reportformat)
    #print(checkbox)

    if reportformat == "PDF":
        return render_pdf(HTML(string=htmlelement))

    if reportformat == "html":
        return htmlelement

# @app.route("/customquery", methods=["GET", "POST"])
# def customquery():
#     form = CustomQueryForm()
#     geojson = GeoRDFStore.wkt2geojson(wktdata)
#
#     if not form.validate_on_submit():
#         #print("Before pressing Submit")
#         return flask.render_template("page_4.html", form=form, prefill=geojson, results="", route="customquery")
#
#     ### WKT Query
#     results = STORE.query_wkt(wktdata)
#
#     query_from_user = form.querryfield.data
#     #print(query_from_user)
#     query_latest = """
#             prefix xsd: <http://www.w3.org/2001/XMLSchema#>
#             PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
#             PREFIX geo: <http://www.opengis.net/ont/geosparql#>
#             PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
#             PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
#             %s
#     """%query_from_user
#
#     tempFound = []
#     error = False
#     try:
#         GeoRDFStore._do_query(query_latest, query_latest)
#     except:
#         error = True
#         print("Error in Query")
#
#     if not error:
#         for result in GeoRDFStore._do_query(query_latest, query_latest):
#             print(result)
#             tempFound.append(result['subject']['value'])
#
#         newresults = []
#         for i in range(len(results)):
#             if results[i] in tempFound and results[i] not in newresults:
#                 print("yes")
#                 newresults.append(results[i])
#             else:
#                 continue
#                 # print("no")
#
#         detailscustomquery = [STORE.get_details(_, ) for _ in newresults]
#         print(detailscustomquery)
#         #print(detailscustomquery)
#
#         if detailscustomquery == []:
#             detailscustomquery = [{"Error": "Empty Results Returned.....Please check the Query again"}]
#
#         return flask.render_template("page_4.html", form=form, prefill=geojson,
#         results=detailscustomquery, route="customquery")
#
#     else:
#         detailscustomquery = [{"Error": "Something Wrong With Query......Please check and submit again"}]
#
#         return flask.render_template("page_4.html", form=form, prefill=geojson, results=detailscustomquery,
#                                      route="customquery")


@app.route("/templates", methods=["GET", "POST"])
def templates():
    if enduser == "agm":
        form = QueryForm1()
    elif enduser == "contractor":
        form = QueryForm2()
    elif enduser == "customer":
        form = QueryForm3()
    else:
        form = QueryForm()

    #wktdata = form.wkt.data
    if wktdata:
        geojson = GeoRDFStore.wkt2geojson(wktdata)
    else:
        return redirect(url_for('hello_world'))

    #print(geojson)
    global htmlelement
    global details

    #print("One")

    if not form.validate_on_submit():
        #geojson = GeoRDFStore.wkt2geojson(form.wkt.data)
        #print(geojson)

        #print("I pressed submit")
        return flask.render_template("page_4.html", form=form, prefill=geojson, selectoption="none", route="templates")

    topicsToQuery = flask.request.form.get('topicsOfInterests')
    #results = STORE.query_wkt(form.wkt.data)
    results = STORE.query_wkt(wktdata)

    form2 = OutputSelectionForm()
    #form2 = SimpleForm()

    if topicsToQuery != "All Activities":
        normal = {'FertilizerDetails':"ApplyingFertilizer", 'HarvestingDetails':"Harvesting", 'CropDetails':"Plantation",
                  'SeedingDetails':"Seeding", 'TillingDetails':"Tilling", 'All Crops':"All Crops"}

        #print("Particular activity")

        topicsToQuery1 = normal[topicsToQuery]

        query1 = query4 % topicsToQuery1

        tempFound = []
        GeoRDFStore._do_query(query1, query1)
        for result in GeoRDFStore._do_query(query1, query1):
            tempFound.append(result['subject']['value'])

        newresults = []
        for i in range(len(results)):
            if results[i] in tempFound and results[i] not in newresults:
                #print("yes")
                newresults.append(results[i])
            else:
                continue
                #print("no")

        #print("checking here..........")
        #print(newresults)
        #print(tempFound)
        details = [STORE.get_details(_,) for _ in newresults]
        detailslatest = details
        htmlelement = flask.render_template("page5.html", form=form2, details=details, route="reports")
        return flask.render_template("page5.html", form=form2, details=details, route="reports")

    details = [STORE.get_details(_,) for _ in results]
    detailslatest = details
    #print(details)
    htmlelement = flask.render_template("page_7.html", form=form2, details=details, route="reports")
    return render_pdf(HTML(string=htmlelement))
    #return flask.render_template("page_7.html", form=form2, details=details, route="reports")
    #html = flask.render_template('page_7.html', details=details)
    #return render_pdf(HTML(string=html))


@app.route("/customreports", methods=["GET", "POST"])
def customreports():

    #print("wktdata")
    #print(wktdata)
    if wktdata is not '':
        resultscustom = STORE.query_wkt(wktdata)
    else:
        return redirect(url_for('choose_end_user'))

    specificvalues = {'TotalAmountOfFertilizerUsedinKGPerHa':'', 'TotalAmountOfFertilizerUsedinDtPerHa':'','AppliedMineralFertilizerAgents':'', \
                      'TotalAmountOfMineralFertilizerNitrogenContentInKgPerHa':'', 'NamesOfPeopleWorkedFertilizer':'', \
                      'HarvestedCrops':'', 'TotalHarvestedQuantityInDtPerHa':'','NamesOfPeopleWorkedHarvesting':'', 'NamesOfMachinesUsedHarvesting':'',\
                      'AppliedTillage':'', 'NamesOfPeopleWorkedTillage':'', 'NamesOfMachinesUsedTillage':'',\
                      'SownCropsInFields':'', 'NamesOfPeopleWorkedPlantation':'','TotalsownQuantityInKgPerHa':'', 'AppliedPesticideAgents':'','TotalPesticideAmountUsedInLPerHa':''
                      }

    fertilizerjournal = []
    harvestedjournal  = []
    tillagejournal = []
    sowncropjournal = []
    seedingjournal =  []


    activities = ["ApplyingFertilizer", "Harvesting", "Plantation", "Seeding", "Tilling"]
    for activity in activities:
        result = GeoRDFStore._do_query((query4 % activity), (query4 % activity))
        if activity == "ApplyingFertilizer":
            for i in result:
                fertilizerjournal.append(i['subject']['value'])
        if activity == "Harvesting":
            for i in result:
                harvestedjournal.append(i['subject']['value'])
        if activity == "Plantation":
            for i in result:
                sowncropjournal.append(i['subject']['value'])
        if activity == "Seeding":
            for i in result:
                seedingjournal.append(i['subject']['value'])
        if activity == "Tilling":
            for i in result:
                tillagejournal.append(i['subject']['value'])

    samplestring = ""
    for logjournal in resultscustom:
        samplestring += "?subject = <%s> || "%(logjournal)

    samplestringfertilizer = ""
    for logjournal in fertilizerjournal:
        samplestringfertilizer += "?subject = <%s> || "%(logjournal)

    samplestringharvesting = ""
    for logjournal in harvestedjournal:
        samplestringharvesting += "?subject = <%s> || " % (logjournal)

    samplestringtillage = ""
    for logjournal in tillagejournal:
        samplestringtillage += "?subject = <%s> || " % (logjournal)

    samplestringplantation = ""
    for logjournal in sowncropjournal:
        samplestringplantation += "?subject = <%s> || " % (logjournal)

    samplestringseeding = ""
    for logjournal in seedingjournal:
        samplestringseeding += "?subject = <%s> || " % (logjournal)

    samplestring = samplestring[:-3]
    samplestringfertilizer = samplestringfertilizer[:-3]
    samplestringharvesting = samplestringharvesting[:-3]
    samplestringtillage = samplestringtillage[:-3]
    samplestringplantation = samplestringplantation[:-3]
    samplestringseeding = samplestringseeding[:-3]

    totalAmountOfFertilizerUsedinKGPerHa = query6 % samplestring
    totalAmountOfFertilizerUsedinDtPerHa = query7 % samplestring
    appliedMineralFertilizerAgents = query8 % samplestring
    totalAmountOfMineralFertilizerNitrogenContentInKgPerHa = query9 % samplestring
    namesOfPeopleWorkedFertilizer = query10 % samplestringfertilizer
    namesOfPeopleWorkedHarvesting = query10 % samplestringharvesting
    namesOfPeopleWorkedTillage = query10 % samplestringtillage
    namesOfPeopleWorkedPlantation = query10 % samplestringplantation
    namesOfPeopleWorkedSeeding = query10 % samplestringseeding
    harvestedCrops = query11 % samplestring
    totalHarvestedQuantityInDtPerHa = query12 % samplestring
    namesofmachinesusedharvesting = query13 % samplestringharvesting
    namesofmachinesusedtillage = query13 % samplestringtillage
    appliedtillage = query14 % samplestring
    sowncropinfields = query15 % samplestring
    totalsownQuantityInKgPerHa = query16 % samplestring
    appliedPesticideAgents = query17 % samplestring
    totalPesticideAmountUsedInLPerHa = query18 % samplestring

    #print(totalfertilzer)

    try:
        resulttotaletilizerinkg = GeoRDFStore._do_query(totalAmountOfFertilizerUsedinKGPerHa, totalAmountOfFertilizerUsedinKGPerHa)
        resulttotaletilizerindt = GeoRDFStore._do_query(totalAmountOfFertilizerUsedinDtPerHa, totalAmountOfFertilizerUsedinDtPerHa)
        resultfertilizeragents = GeoRDFStore._do_query(appliedMineralFertilizerAgents, appliedMineralFertilizerAgents)
        resultmineralfertilizernitrogencontent = GeoRDFStore._do_query(totalAmountOfMineralFertilizerNitrogenContentInKgPerHa, totalAmountOfMineralFertilizerNitrogenContentInKgPerHa)
        resultnoofpeoplefertilizer = GeoRDFStore._do_query(namesOfPeopleWorkedFertilizer, namesOfPeopleWorkedFertilizer)
        resultnoofpeopleharvesting = GeoRDFStore._do_query(namesOfPeopleWorkedHarvesting, namesOfPeopleWorkedHarvesting)
        resultnoofpeopletillage = GeoRDFStore._do_query(namesOfPeopleWorkedTillage, namesOfPeopleWorkedTillage)
        resultnoofpeopleplantation = GeoRDFStore._do_query(namesOfPeopleWorkedPlantation, namesOfPeopleWorkedPlantation)
        resultnoofpeopleseeding = GeoRDFStore._do_query(namesOfPeopleWorkedSeeding, namesOfPeopleWorkedSeeding)
        resultharvestedcrops = GeoRDFStore._do_query(harvestedCrops, harvestedCrops)
        resultharvestedquantity = GeoRDFStore._do_query(totalHarvestedQuantityInDtPerHa, totalHarvestedQuantityInDtPerHa)
        resultmachinesusedharvesting = GeoRDFStore._do_query(namesofmachinesusedharvesting, namesofmachinesusedharvesting)
        resultmachinesusedtillage = GeoRDFStore._do_query(namesofmachinesusedtillage, namesofmachinesusedtillage)
        resultappliedtillage = GeoRDFStore._do_query(appliedtillage, appliedtillage)
        resultsowncropinfields = GeoRDFStore._do_query(sowncropinfields, sowncropinfields)
        resulttotalsownQuantityInKgPerHa = GeoRDFStore._do_query(totalsownQuantityInKgPerHa, totalsownQuantityInKgPerHa)
        resultappliedPesticideAgents = GeoRDFStore._do_query(appliedPesticideAgents, appliedPesticideAgents)
        resulttotalPesticideAmountUsedInLPerHa = GeoRDFStore._do_query(totalPesticideAmountUsedInLPerHa, totalPesticideAmountUsedInLPerHa)

        resultfertilizeragentslatest = []
        for fertagents in resultfertilizeragents:
            resultfertilizeragentslatest.append(fertagents['object']['value'])

        resultnoofpeoplelatestfertilizerlatest = []
        for people in resultnoofpeoplefertilizer:
            resultnoofpeoplelatestfertilizerlatest.append(people['object']['value'])

        resultnoofpeopleharvestinglatest = []
        for people in resultnoofpeopleharvesting:
            resultnoofpeopleharvestinglatest.append(people['object']['value'])

        resultnoofpeopletillagelatest = []
        for people in resultnoofpeopletillage:
            resultnoofpeopletillagelatest.append(people['object']['value'])

        resultnoofpeopleplantationlatest = []
        for people in resultnoofpeopleplantation:
            resultnoofpeopleplantationlatest.append(people['object']['value'])

        resultnoofpeopleseedinglatest = []
        for people in resultnoofpeopleseeding:
            resultnoofpeopleseedinglatest.append(people['object']['value'])

        resultharvestedcropslatest = []
        for crops in resultharvestedcrops:
            resultharvestedcropslatest.append(crops['object']['value'])

        resultmachinesusedharvestinglatest = []
        for machine in resultmachinesusedharvesting:
            resultmachinesusedharvestinglatest.append(machine['object']['value'])

        resultmachinesusedtillagelatest = []
        for machine in resultmachinesusedtillage:
            resultmachinesusedtillagelatest.append(machine['object']['value'])

        resultappliedtillagelatest = []
        for tillage in resultappliedtillage:
            resultappliedtillagelatest.append(tillage['object']['value'])

        resultsowncropinfieldslatest = []
        for crops in resultsowncropinfields:
            resultsowncropinfieldslatest.append(crops['object']['value'])

        resultappliedPesticideAgentslatest = []
        for agents in resultappliedPesticideAgents:
            resultappliedPesticideAgentslatest.append(agents['object']['value'])

        specificvalues['TotalAmountOfFertilizerUsedinKGPerHa'] = resulttotaletilizerinkg[0]['total']['value']
        specificvalues['TotalAmountOfFertilizerUsedinDtPerHa'] = resulttotaletilizerindt[0]['total']['value']
        specificvalues['AppliedMineralFertilizerAgents'] = resultfertilizeragentslatest
        specificvalues['NamesOfPeopleWorkedFertilizer'] = resultnoofpeoplelatestfertilizerlatest
        specificvalues['NamesOfPeopleWorkedHarvesting'] = resultnoofpeopleharvestinglatest
        specificvalues['NamesOfPeopleWorkedPlantation'] = resultnoofpeopleplantationlatest
        specificvalues['NamesOfPeopleWorkedTillage'] = resultnoofpeopletillagelatest
        specificvalues['TotalAmountOfMineralFertilizerNitrogenContentInKgPerHa'] = \
        resultmineralfertilizernitrogencontent[0]['total']['value']
        specificvalues['HarvestedCrops'] = resultharvestedcropslatest
        specificvalues['TotalHarvestedQuantityInDtPerHa'] = resultharvestedquantity[0]['total']['value']
        specificvalues['NamesOfMachinesUsedHarvesting'] = resultmachinesusedharvestinglatest
        specificvalues['NamesOfMachinesUsedTillage'] = resultmachinesusedtillagelatest
        specificvalues['AppliedTillage'] = resultappliedtillagelatest
        specificvalues['SownCropsInFields'] = resultsowncropinfieldslatest
        specificvalues['TotalsownQuantityInKgPerHa'] = resulttotalsownQuantityInKgPerHa[0]['total']['value']
        specificvalues['AppliedPesticideAgents'] = resultappliedPesticideAgentslatest
        specificvalues['TotalPesticideAmountUsedInLPerHa'] = resulttotalPesticideAmountUsedInLPerHa[0]['total']['value']
    except:
        print("Got error here")
        pass

    #print(specificvalues)

    '''All details for activities'''
    detailscustom = [STORE.get_details(_, ) for _ in resultscustom]


    if enduser == "agm":
        topicsOfInterest = ['FertilizerDetails', 'HarvestingDetails', 'TillingDetails']
    elif enduser == "contractor":
        topicsOfInterest = ['CropDetails', 'SeedingDetails', 'TillingDetails']
    elif enduser == "customer":
        topicsOfInterest = ['FertilizerDetails', 'HarvestingDetails', 'CropDetails', 'SeedingDetails']
    else:
        topicsOfInterest = []

    if request.method == "POST":
        interestedtopics = request.form.getlist('hello')
        #print(interestedtopics)

        interestedtopics1 = request.form.getlist('fertilizer')
        interestedtopics2 = request.form.getlist('harvesting')
        interestedtopics3 = request.form.getlist('crop')
        interestedtopics4 = request.form.getlist('tilling')
        interestedtopics5 = request.form.getlist('seeding')

        for i in interestedtopics2:
            interestedtopics1.append(i)

        for i in interestedtopics3:
            interestedtopics1.append(i)

        for i in interestedtopics4:
            interestedtopics1.append(i)

        for i in interestedtopics5:
            interestedtopics1.append(i)

        #print(interestedtopics1)

        detailscustomupdate = []
        for i in detailscustom:
            #print(i['activity'])
            if i['activity'] in interestedtopics1:
                detailscustomupdate.append(i)
            else:
                continue

        othervalues = { }
        activities = ["ApplyingFertilizer", "Harvesting", "Tilling", "Seeding", "Plantation"]

        for values in interestedtopics1:
            if values not in activities:
                othervalues[values] = specificvalues[values]
            else:
                pass

        print(othervalues)

        #print(detailscustomupdate)

        outputformat = request.form.get('options')
        #print(outputformat)

        if outputformat == "PDF":
            html = flask.render_template("page_10.html", details=detailscustomupdate, details1=othervalues, user_image=file_path)
            return render_pdf(HTML(string=html))

        if outputformat == "HTML":
            return flask.render_template("page_10.html", details=detailscustomupdate, details1=othervalues, user_image=file_path, html=True)

    return flask.render_template("page_9.html", details=detailscustom, user_image=file_path, specific_value=specificvalues, \
                                 list=topicsOfInterest, route="customreports")


@app.route('/page8', methods=['GET', 'POST'])
def view_report():
    return render_template("page8.html")


if __name__ == "__main__":
    print('Inside main')
    GRAPH = rl.Graph(BACKEND, identifier="http://project/base/defaultgraph")
    print(GRAPH)
    SECRET_KEY = "secret"
    STORE.assign_graph(GRAPH)
    print(STORE)
    #GRAPH.parse(RDF_SOURCE)
    app.run(debug=True)



#) TODO Please Add Validations
#) TODO Selection of Different Output formats
#) TODO Selection of Different Templates
#) TODO removing un wanted code
#) TODO store error
#) TODO Intersection of polygons                                        ( Done )
#) TODO Login Restriction
#) TODO Template selection
#) TODO Include screenshot in reports                                   ( Done )
#) TODO Work on Different Templates
#) TODO DIfferent use cases ( DO research and add it to the template )


#) This week
#) TODO dynamic handling of varibales
#) TODO Topics editiing
#) TODO Templates deletion
#) TODO Template editing
#) TODO Visualization Of Templates ( Editing ) Option
#)

#) TODO : RDF Graph visualization
#) TODO : GEOSPARQL QUERIES
#) TODO : CREATING TEMPLATE FILE


#) TODO : imageconverter, bootstrap, pdf creater

#) TODO : activities json - done
#) TODO : geosparql
#) TODO : templatejson


#) TODO : Query value modification should be executed in SPARQL endpoint
#) TODO : Back press in PDF
#) TODO Location Filter in Query
#) TODO : more references
#) TODO : complete testing
