topicsofinterestDictionary = {}

query1 = """
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX : <http://example.org/#>

SELECT distinct ?subject ?wkt
FROM <http://project/base/default>
WHERE { 
 ?subject geo:asWKT ?wkt .

}"""

query2 = """
prefix xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT distinct ?predicate ?object
WHERE {
                GRAPH <http://project/base/default> {
                <%s> ?predicate ?object
                }
            }"""


query3 = """
            prefix xsd: <http://www.w3.org/2001/XMLSchema#>

            SELECT distinct ?subject ?predicate ?object
            WHERE {
                GRAPH <http://project/base/default> {
                ?subject ?predicate ?object FILTER (  ?subject = <%s> ) .
                }
            }"""


query4 = """
            prefix xsd: <http://www.w3.org/2001/XMLSchema#>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX geo: <http://www.opengis.net/ont/geosparql#>
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX : <http://example.org/#>

            SELECT distinct ?subject
            WHERE {
            GRAPH <http://project/base/default> {
            ?subject ?predicate ?object FILTER ( str(?object) = \"%s\" ) . 
            }
            }
            """

query5 =  """
                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX geo: <http://www.opengis.net/ont/geosparql#>
                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                %s
                """

query6 = """PREFIX : <http://example.org/#> 
                        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

                        R
                         """

query7 = """PREFIX : <http://example.org/#> 
                        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

                        SELECT (sum(xsd:float(?object)) AS ?total)
                        WHERE { 
                        GRAPH <http://project/base/default> { 
                         ?subject ?predicate ?object FILTER (( %s ) && ( ?predicate = :appliedFertilizerAmountInDtPerHa ) )
                        } }
                         """

query8 = """PREFIX : <http://example.org/#> 
                        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

                        SELECT ?object
                        WHERE { 
                        GRAPH <http://project/base/default> { 
                         ?subject ?predicate ?object FILTER (( %s ) && ( ?predicate = :appliedMineralFertilizerAgent ) )
                        } }
                         """

query9 = """PREFIX : <http://example.org/#> 
                        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

                        SELECT (sum(xsd:float(?object)) AS ?total)
                        WHERE { 
                        GRAPH <http://project/base/default> { 
                         ?subject ?predicate ?object FILTER (( %s ) && ( ?predicate = :appliedMineralFertilizerNitrogenContentInKgPerHa ) )
                        } }
                         """

query10 = """PREFIX : <http://example.org/#> 
                        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

                        SELECT ?object
                        WHERE { 
                        GRAPH <http://project/base/default> { 
                         ?subject ?predicate ?object FILTER (( %s ) && ( ?predicate = :Personname ))
                        } }
                         """

query11 = """PREFIX : <http://example.org/#> 
                        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

                        SELECT ?object
                        WHERE { 
                        GRAPH <http://project/base/default> { 
                         ?subject ?predicate ?object FILTER (( %s ) && ( ?predicate = :harvestedCrop ) )
                        } }
                         """

query12 = """PREFIX : <http://example.org/#> 
                        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

                        SELECT (sum(xsd:float(?object)) AS ?total)
                        WHERE { 
                        GRAPH <http://project/base/default> { 
                         ?subject ?predicate ?object FILTER (( %s ) && ( ?predicate = :harvestedQuantityInDtPerHa ) )
                        } }
                         """

query13 = """PREFIX : <http://example.org/#> 
                        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

                        SELECT ?object
                        WHERE { 
                        GRAPH <http://project/base/default> { 
                         ?subject ?predicate ?object FILTER (( %s ) && ( ?predicate = :Machinename ) )
                        } }
                         """

query14 = """PREFIX : <http://example.org/#> 
                        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

                        SELECT ?object
                        WHERE { 
                        GRAPH <http://project/base/default> { 
                         ?subject ?predicate ?object FILTER (( %s ) && ( ?predicate = :appliedTillage ) )
                        } }
                         """


query15 = """PREFIX : <http://example.org/#> 
                        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

                        SELECT ?object
                        WHERE { 
                        GRAPH <http://project/base/default> { 
                         ?subject ?predicate ?object FILTER (( %s ) && ( ?predicate = :sownCrop ) )
                        } }
                         """

query16 = """PREFIX : <http://example.org/#> 
                        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

                        SELECT (sum(xsd:float(?object)) AS ?total)
                        WHERE { 
                        GRAPH <http://project/base/default> { 
                         ?subject ?predicate ?object FILTER (( %s ) && ( ?predicate = :sownQuantityInKgPerHa ) )
                        } }
                         """


query17 = """PREFIX : <http://example.org/#> 
                        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

                        SELECT ?object
                        WHERE { 
                        GRAPH <http://project/base/default> { 
                         ?subject ?predicate ?object FILTER (( %s ) && ( ?predicate = :appliedPesticideAgent ) )
                        } }
                         """

query18 = """PREFIX : <http://example.org/#> 
                        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

                        SELECT (sum(xsd:float(?object)) AS ?total)
                        WHERE { 
                        GRAPH <http://project/base/default> { 
                         ?subject ?predicate ?object FILTER (( %s ) && ( ?predicate = :appliedPesticideAmountInLPerHa ) )
                        } }
                         """

query19 = """PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX : <http://example.org/#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX spatial: <http://jena.apache.org/spatial#>
SELECT ?subject
WHERE {
GRAPH <http://project/base/defaultgraph> {
    ?subject ?predicate ?object . FILTER (?object >= xsd:date("%s") && ?object <= xsd:date("%s"))
  }}
"""